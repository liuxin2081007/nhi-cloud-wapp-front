package net.zigin.nhi.waste.view.depotin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;

/**
 * 医废入库界面
 */
public class DepotInActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mScanLayout;
    private LinearLayout mBatchLayout;
    private LinearLayout mBoxLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depot_in);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScanLayout = findViewById(R.id.scan_layout);
        mBatchLayout = findViewById(R.id.batch_layout);
        mBoxLayout = findViewById(R.id.box_layout);

        mScanLayout.setOnClickListener(this);
        mBatchLayout.setOnClickListener(this);
        mBoxLayout.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_layout:  //扫码入库
                Intent intent1 = new Intent(DepotInActivity.this, ScanWasteActivity.class);
                intent1.putExtra("scan_out", "scan_in");
                startActivity(intent1);
                break;
            case R.id.batch_layout:  //批量入库
                Intent intent2 = new Intent(DepotInActivity.this, BatchInActivity.class);
                startActivity(intent2);
                break;
            case R.id.box_layout:   //箱袋绑定入库
                Intent intent3 = new Intent(DepotInActivity.this, ScanBoxActivity.class);
                startActivity(intent3);
                break;
        }
    }
}