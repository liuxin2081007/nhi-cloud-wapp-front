package net.zigin.nhi.waste.view.depotout;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class AddReceiverActivity extends AppCompatActivity {

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch mDefaultReceiver;
    private EditText mName;
    private EditText mPhone;
    private EditText mCarNumber;
    private Button mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receiver);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mName = findViewById(R.id.name);
        mPhone = findViewById(R.id.phone);
        mCarNumber = findViewById(R.id.car_number);
        mDefaultReceiver = findViewById(R.id.default_receiver);
        mSave = findViewById(R.id.save);
    }

    private void initData() {
        mSave.setOnClickListener(v -> {
            if (StringUtil.isEmpty(mName.getText().toString()) && StringUtil.isEmpty(mPhone.getText().toString()) && StringUtil.isEmpty(mCarNumber.getText().toString())) {
                Toast.makeText(AddReceiverActivity.this, "请填写完整信息！", Toast.LENGTH_SHORT).show();
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("realName", mName.getText().toString());
                map.put("mobile", mPhone.getText().toString());
                map.put("carCode", mCarNumber.getText().toString());
                map.put("isDefault", mDefaultReceiver.isChecked() ? 1 : 0);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .saveReceiver(Paper.book().read(Constants.TOKEN), map);
                DialogManager.getInstance().showLoading(this);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        Log.i("添加接收人", data);
                        Toast.makeText(AddReceiverActivity.this, "保存成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });

    }

}

