package net.zigin.nhi.waste.bean;

/**
 * 打印信息bean
 */
public class PrintMsgBean {
    private String code;
    private String wasteClassifyName;
    private String weight;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "{" +
                "code='" + code + '\'' +
                ", wasteClassifyName='" + wasteClassifyName + '\'' +
                ", weight='" + weight + '\'' +
                '}';
    }
}
