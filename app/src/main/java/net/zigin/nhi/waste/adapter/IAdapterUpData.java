package net.zigin.nhi.waste.adapter;

import java.util.List;

/**
 * adapter通知activity刷新界面
 */
public interface IAdapterUpData {
    void upDataUi(List<String> list);
}
