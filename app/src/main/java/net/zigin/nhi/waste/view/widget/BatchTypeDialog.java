package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.FilterDepartAdapter;
import net.zigin.nhi.waste.adapter.IAnnounceTypeData;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class BatchTypeDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvType;
    private Button mConfirm;
    private Activity activity;
    private IAnnounceTypeData dd;
    public static BatchTypeDialog mInstance;

    private List<String> typeList = new ArrayList<>();
    private String type = "";

    public BatchTypeDialog(@NonNull Context context, Activity activity, IAnnounceTypeData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static BatchTypeDialog getInstance(Context context, Activity activity, IAnnounceTypeData dd) {
        if (mInstance == null) {
            synchronized (BatchTypeDialog.class) {
                if (mInstance == null) {
                    mInstance = new BatchTypeDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_waste_type);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvType = findViewById(R.id.rv_type);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvType.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    @SuppressLint("NotifyDataSetChanged")
    private void initData() {
        typeList.add("全部");
        typeList.add("感染性医废");
        typeList.add("损伤性医废");
        typeList.add("病理性医废");
        typeList.add("药物性医废");
        typeList.add("化学性医废");
        FilterDepartAdapter departAdapter = new FilterDepartAdapter(typeList);
        mRvType.setAdapter(departAdapter);
        departAdapter.setGetListener((position, text) -> {
            departAdapter.setPosition(position);
            departAdapter.notifyDataSetChanged();

            switch (typeList.get(position)) {
                case "全部":
                    type = "all";
                    break;
                case "化学性医废":
                    type = "chemical";
                    break;
                case "感染性医废":
                    type = "infection";
                    break;
                case "损伤性医废":
                    type = "injury";
                    break;
                case "病理性医废":
                    type = "pathology";
                    break;
                case "药物性医废":
                    type = "drug";
                    break;
            }
        });

        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mConfirm.setOnClickListener(v -> {
            if (StringUtil.isNotEmpty(type)) {
                dd.upDataUi(type);
            }
            dismiss();
        });
    }
}
