package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.FilterDepartAdapter;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.bean.DepartBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class WasteTypeDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvType;
    private Button mConfirm;
    private Activity activity;
    private IDialogUpData dd;
    public static WasteTypeDialog mInstance;

    private List<String> typeList = new ArrayList<>();
    private String type = "";

    public WasteTypeDialog(@NonNull Context context, Activity activity, IDialogUpData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static WasteTypeDialog getInstance(Context context, Activity activity, IDialogUpData dd) {
        if (mInstance == null) {
            synchronized (WasteTypeDialog.class) {
                if (mInstance == null) {
                    mInstance = new WasteTypeDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_waste_type);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvType = findViewById(R.id.rv_type);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvType.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    private void initData() {
        typeList.add("化学性医废");
        typeList.add("感染性医废");
        typeList.add("损伤性医废");
        typeList.add("病理性医废");
        typeList.add("药物性医废");
        FilterDepartAdapter departAdapter = new FilterDepartAdapter(typeList);
        mRvType.setAdapter(departAdapter);
        departAdapter.setGetListener((position, text) -> {
            departAdapter.setPosition(position);
            departAdapter.notifyDataSetChanged();

            type = typeList.get(position);
        });

        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mConfirm.setOnClickListener(v -> {
            if (StringUtil.isNotEmpty(type)) {
                dd.upDataUi(type, "");
            }
            dismiss();
        });
    }
}
