package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import net.zigin.nhi.waste.R;

import java.util.ArrayList;
import java.util.List;

public class NotifyDetailAdapter extends RecyclerView.Adapter<NotifyDetailAdapter.ViewHolder> {

    public List<String> list;

    public NotifyDetailAdapter(List<String> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<String> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notify_detail, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String bean = list.get(position);
        Glide.with(holder.mImg).load(bean).into(holder.mImg);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.img);
        }
    }
}
