package net.zigin.nhi.waste.view.collect;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.MainApplication;
import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.scan.CodeListener;
import net.zigin.nhi.waste.scan.CodeReceiver;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.main.LoginActivity;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;


/**
 * 医废收集科室扫码界面
 */
public class OfficeScanActivity extends AppCompatActivity implements CodeListener {

    private CodeReceiver receiver;
    private LinearLayout mScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_office_scan);

        initView();
        initReceiver();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }

    private void initReceiver() {
        receiver = new CodeReceiver();
        receiver.setListener(this);
        registerReceiver(receiver, new IntentFilter("com.barcode.sendBroadcast"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (MainApplication.scanManager != null) {
                //开启扫描
                Intent intent = new Intent();
                intent.setAction("com.barcode.sendBroadcastScan");
                sendBroadcast(intent);
            } else {
                Intent intent = new Intent(OfficeScanActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 跳转页面
     *
     * @param qrCode 扫码结果
     */
    private void jump(String qrCode) {
        if(CommonUtils.isFastClick()) {
            return;
        }
        if (StringUtil.isNotEmpty(qrCode)) {
            Map<String, Object> map = new HashMap<>();
            map.put("content", qrCode);

            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getOrCodeType(Paper.book().read(Constants.TOKEN).toString(), map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("医废收集", data);
                    if ("hospitalDepart".equals(data)) {
                        Intent intent = new Intent(OfficeScanActivity.this, WasteCollectActivity.class);
                        intent.putExtra("scan_result", qrCode);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(OfficeScanActivity.this, "科室二维码扫描错误", Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }
    }

    @Override
    public void getData(String data) {
        Log.i("扫码结果", data);
        jump(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                jump(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
            System.gc();
        }
        super.onDestroy();
    }
}