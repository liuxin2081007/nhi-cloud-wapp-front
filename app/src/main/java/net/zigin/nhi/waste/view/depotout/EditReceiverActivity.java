package net.zigin.nhi.waste.view.depotout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 编辑接收人界面
 */
public class EditReceiverActivity extends AppCompatActivity {

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch mDefaultReceiver;
    private TitleBarLayout mTitle;
    private EditText mName;
    private EditText mPhone;
    private EditText mCarNumber;
    private Button mSave;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_receiver);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mName = findViewById(R.id.name);
        mPhone = findViewById(R.id.phone);
        mCarNumber = findViewById(R.id.car_number);
        mDefaultReceiver = findViewById(R.id.default_receiver);
        mSave = findViewById(R.id.save);
    }

    private void initData() {
        Intent intent = getIntent();
        id = intent.getStringExtra("receiver_id");
        getReceiverMessage();

        mTitle.setRightTextClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditReceiverActivity.this);
            builder.setTitle("提示：");
            builder.setMessage("是否删除该接收人？");
            builder.setCancelable(true);            //点击对话框以外的区域是否让对话框消失

            //设置正面按钮
            builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteReceiver();
                    dialog.dismiss();
                }
            });
            //设置反面按钮
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        mSave.setOnClickListener(v -> {
            if (StringUtil.isEmpty(mName.getText().toString()) && StringUtil.isEmpty(mPhone.getText().toString()) && StringUtil.isEmpty(mCarNumber.getText().toString())) {
                Toast.makeText(EditReceiverActivity.this, "请填写完整信息！", Toast.LENGTH_SHORT).show();
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("id", id);
                map.put("realName", mName.getText().toString());
                map.put("mobile", mPhone.getText().toString());
                map.put("carCode", mCarNumber.getText().toString());
                map.put("isDefault", mDefaultReceiver.isChecked() ? 1 : 0);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .saveReceiver(Paper.book().read(Constants.TOKEN), map);
                DialogManager.getInstance().showLoading(this);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        Log.i("添加接收人", data);
                        Toast.makeText(EditReceiverActivity.this, "保存成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
    }

    /**
     * 删除接收人
     */
    private void deleteReceiver() {
        if (StringUtil.isNotEmpty(id)) {
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .removeReceiver(Paper.book().read(Constants.TOKEN), id);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Toast.makeText(EditReceiverActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }

    /**
     * 获取接收人信息
     */
    private void getReceiverMessage() {
        if (StringUtil.isNotEmpty(id)) {
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getReceiverById(Paper.book().read(Constants.TOKEN), id);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("接收人信息", data);
                    JSONObject object = JSON.parseObject(data);
                    mName.setText(object.getString("realName"));
                    mPhone.setText(object.getString("mobile"));
                    mCarNumber.setText(object.getString("carCode"));
                    mDefaultReceiver.setChecked("1".equals(object.getString("isDefault")));
                }
            });
        }

    }
}