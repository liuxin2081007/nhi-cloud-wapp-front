package net.zigin.nhi.waste.bean;

public class BoxOutBean {
    private String code;
    private String collectWeight;
    private String content;
    private String hospitalBaseName;
    private String hospitalDepartId;
    private String hospitalDepartName;
    private String id;
    private String realClassifyCode;
    private String status;
    private String wasteBoxRecordId;
    private String wasteClassifyCode;
    private String wasteClassifyName;
    private String wasteCount;
    private String weight;

    public String getRealClassifyCode() {
        return realClassifyCode;
    }

    public void setRealClassifyCode(String realClassifyCode) {
        this.realClassifyCode = realClassifyCode;
    }

    public String getCollectWeight() {
        return collectWeight;
    }

    public void setCollectWeight(String collectWeight) {
        this.collectWeight = collectWeight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartId() {
        return hospitalDepartId;
    }

    public void setHospitalDepartId(String hospitalDepartId) {
        this.hospitalDepartId = hospitalDepartId;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWasteBoxRecordId() {
        return wasteBoxRecordId;
    }

    public void setWasteBoxRecordId(String wasteBoxRecordId) {
        this.wasteBoxRecordId = wasteBoxRecordId;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWasteCount() {
        return wasteCount;
    }

    public void setWasteCount(String wasteCount) {
        this.wasteCount = wasteCount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
