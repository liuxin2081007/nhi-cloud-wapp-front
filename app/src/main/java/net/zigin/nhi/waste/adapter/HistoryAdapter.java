package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BatchInBean;
import net.zigin.nhi.waste.bean.HistoryBean;
import net.zigin.nhi.waste.bean.NotifyBean;
import net.zigin.nhi.waste.view.history.RecordDetailActivity;
import net.zigin.nhi.waste.view.notify.NotifyDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{

    public List<HistoryBean> list;

    public HistoryAdapter(List<HistoryBean> list) {
        this.list = list;
    }

    public void notifyData(List<HistoryBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryBean bean = list.get(position);
        holder.mNumber.setText(bean.getCode());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mWeight.setText(bean.getWeight());
        holder.mHandover.setText(bean.getHandUserStaffName());
        holder.mTime.setText(bean.getCreateTime());

        switch (bean.getWasteClassifyCode()) {
            case "chemical":
                holder.mType.setText("化学性");
                break;
            case "drug":
                holder.mType.setText("药物性");
                break;
            case "infection":
                holder.mType.setText("感染性");
                break;
            case "injury":
                holder.mType.setText("损伤性");
                break;
            case "pathology":
                holder.mType.setText("病理性");
                break;
        }

        switch (bean.getStatus()) {
            case "out_depot":
                holder.mStatus.setText("已出库");
                holder.mStatus.setBackgroundResource(R.drawable.bg_out_storage);
                holder.mCkLayout.setVisibility(View.VISIBLE);
                holder.mCkCode.setText(bean.getCkCode());
                break;
            case "do_collect":
                holder.mStatus.setText("未入库");
                holder.mStatus.setBackgroundResource(R.drawable.bg_not_in_storage);
                holder.mCkLayout.setVisibility(View.GONE);
                break;
            case "in_depot":
                holder.mStatus.setText("已入库");
                holder.mStatus.setBackgroundResource(R.drawable.bg_not_in_storage);
                holder.mCkLayout.setVisibility(View.GONE);
                break;
        }

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), RecordDetailActivity.class);
            intent.putExtra("recordID", bean.getId());
            v.getContext().startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mNumber;
        private TextView mDepartName;
        private TextView mType;
        private TextView mWeight;
        private TextView mHandover;
        private TextView mTime;
        private TextView mStatus;
        private LinearLayout mCkLayout;
        private TextView mCkCode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNumber = itemView.findViewById(R.id.number);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mWeight = itemView.findViewById(R.id.weight);
            mHandover = itemView.findViewById(R.id.handover);
            mTime = itemView.findViewById(R.id.time);
            mStatus = itemView.findViewById(R.id.status);
            mCkLayout = itemView.findViewById(R.id.ck_layout);
            mCkCode = itemView.findViewById(R.id.ckCode);
        }
    }
}
