package net.zigin.nhi.waste.bean;

import android.graphics.Bitmap;

public class BitmapBean {
    private Bitmap bitmap;
    private boolean isUpload;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isUpload() {
        return isUpload;
    }

    public void setUpload(boolean upload) {
        isUpload = upload;
    }
}
