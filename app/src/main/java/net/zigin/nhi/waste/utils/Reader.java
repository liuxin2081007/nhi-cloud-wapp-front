package net.zigin.nhi.waste.utils;

import android.annotation.SuppressLint;
import android.widget.TextView;

import com.rfid.trans2000.UHFLib;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Reader {
    public static UHFLib rrlib = null;

    public static void writeLog(String log, TextView tvResult) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");// HH:mm:ss
        Date date = new Date(System.currentTimeMillis());
        String textlog = simpleDateFormat.format(date) + " " + log;
        tvResult.setText(textlog);
    }
}