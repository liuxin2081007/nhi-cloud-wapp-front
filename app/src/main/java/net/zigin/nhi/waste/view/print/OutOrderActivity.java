package net.zigin.nhi.waste.view.print;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.adapter.OutOrderAdapter;
import net.zigin.nhi.waste.bean.OutOrderBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 出库单界面
 */
public class OutOrderActivity extends AppCompatActivity {

    private SmartRefreshLayout mRefresh;
    private RecyclerView mRvOrder;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private OutOrderAdapter adapter;
    private List<OutOrderBean> list = new ArrayList<>();

    private int page = 1;
    private int limit = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_order);

        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mRefresh = findViewById(R.id.refresh);
        mRvOrder = findViewById(R.id.rv_order);
        mRvOrder.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initData() {
        mRefresh.autoRefresh();
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                list.clear();
                page = 1;
                getOutOrderList();
            }
        });

        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                Map<String, Object> map = new HashMap<>();
                map.put("isPage", true);
                map.put("pageIndex", page);
                map.put("pageSize", limit);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getWasteOutList(Paper.book().read(Constants.TOKEN), map);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        if (StringUtil.isNotEmpty(data)) {
                            Log.i("出库单列表", data);
                            List<OutOrderBean> beanList = JSONArray.parseArray(data, OutOrderBean.class);
                            if (beanList != null && beanList.size() > 0) {
                                list.addAll(beanList);
                                adapter.notifyData(list);
                            }
                            if (list.size() < limit) {
                                mRefresh.finishLoadMoreWithNoMoreData();
                            } else {
                                mRefresh.finishLoadMore();
                                page++;
                            }
                        } else {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        }
                    }
                });
            }
        });
    }

    /**
     * 获取出库单列表
     */
    private void getOutOrderList() {
        Map<String, Object> map = new HashMap<>();
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getWasteOutList(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("出库单列表", data);
                    list = JSONArray.parseArray(data, OutOrderBean.class);
                    if (list != null && list.size() > 0) {
                        adapter = new OutOrderAdapter(list);
                        mRvOrder.setAdapter(adapter);
                        page++;
                    } else {
                        mRvOrder.setAdapter(noDataAdapter);
                    }
                } else {
                    mRvOrder.setAdapter(noDataAdapter);
                }
                mRefresh.finishRefresh();
            }

            @Override
            public void error(String msg) {
                super.error(msg);
                mRefresh.finishRefresh();
                mRvOrder.setAdapter(noDataAdapter);
            }
        });
    }
}