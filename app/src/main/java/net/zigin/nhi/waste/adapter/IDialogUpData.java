package net.zigin.nhi.waste.adapter;

/**
 * dialog通知activity刷新界面
 */
public interface IDialogUpData {
    void upDataUi(String data, String id);
}
