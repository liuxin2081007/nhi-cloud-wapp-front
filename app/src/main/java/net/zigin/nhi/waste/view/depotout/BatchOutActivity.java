package net.zigin.nhi.waste.view.depotout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BatchInAdapter;
import net.zigin.nhi.waste.adapter.BatchOutAdapter;
import net.zigin.nhi.waste.adapter.IActivityUpData;
import net.zigin.nhi.waste.adapter.IAnnounceTypeData;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.BatchInBean;
import net.zigin.nhi.waste.bean.BatchOutBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotin.BatchInActivity;
import net.zigin.nhi.waste.view.widget.BatchTypeDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 批量出库界面
 */
public class BatchOutActivity extends AppCompatActivity {

    private TitleBarLayout mTitle;
    private SmartRefreshLayout mRefresh;
    private RecyclerView mRv;
    private ImageView mImg;
    private Button mDepot;

    private BatchOutAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private List<BatchOutBean> beanList = new ArrayList<>();
    private boolean isSelected = false;

    private List<String> wasteIDs = new ArrayList<>();
    private List<String> wasteTypes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch_out);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRefresh.autoRefresh();
        initData();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mRefresh = findViewById(R.id.refresh);
        mRv = findViewById(R.id.rv);
        mImg = findViewById(R.id.img);
        mDepot = findViewById(R.id.depot);

        mRv.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * 选择类型返回的数据更新adapter
     */
    private IAnnounceTypeData typeDD = new IAnnounceTypeData() {
        @Override
        public void upDataUi(String type) {
            if (StringUtil.isNotEmpty(type)) {
                if (type.equals("all")) {
                    isSelected = true;
                    mImg.setImageResource(R.drawable.depot_in_selected);

                    for (BatchOutBean bean : beanList) {
                        bean.setSelect(true);
                    }
                    if (adapter != null) {
                        adapter.notifyData(beanList);
                    }
                } else {
                    if (beanList != null && beanList.size() > 0) {
                        for (BatchOutBean batchOutBean : beanList) {
                            if (batchOutBean.getWasteClassifyCode().equals(type)) {
                                batchOutBean.setSelect(true);
                            } else {
                                batchOutBean.setSelect(false);
                            }
                        }
                        if (adapter != null) {
                            adapter.notifyData(beanList);
                        }
                    }
                }
            }
        }
    };

    /**
     * 子项取消选中时，全选按钮变更状态为未选中
     */
    private IActivityUpData dd = new IActivityUpData() {
        @Override
        public void upDataUi() {
            isSelected = false;
            mImg.setImageResource(R.drawable.depot_in_unselected);
        }
    };

    /**
     * 获取所有未出库的医废详情，设置全选点击事件
     */
    private void initData() {
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                beanList.clear();
                Observable<BaseResponse<String>> observable1 = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getPendingOutStorageList(Paper.book().read(Constants.TOKEN));
                HttpRetrofitClient.execute(observable1, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        if (StringUtil.isNotEmpty(data)) {
                            Log.i("待出库详情", data);
                            beanList = JSONArray.parseArray(data, BatchOutBean.class);
                            if (beanList != null && beanList.size() > 0) {
                                adapter = new BatchOutAdapter(beanList, dd);
                                mRv.setAdapter(adapter);
                            } else {
                                mRv.setAdapter(noDataAdapter);
                            }
                        } else {
                            mRv.setAdapter(noDataAdapter);
                        }
                        mRefresh.finishRefresh();
                    }

                    @Override
                    public void error(String msg) {
                        super.error(msg);
                        mRefresh.finishRefresh();
                        mRv.setAdapter(noDataAdapter);
                    }
                });
            }
        });

        mTitle.setRightTextClickListener(v -> {
            BatchTypeDialog dialog = BatchTypeDialog.getInstance(this, BatchOutActivity.this, typeDD);
            dialog.show();
        });

        mImg.setOnClickListener(v -> {
            if (isSelected) {
                isSelected = false;
                mImg.setImageResource(R.drawable.depot_in_unselected);

                for (BatchOutBean bean : beanList) {
                    bean.setSelect(false);
                }
            } else {
                isSelected = true;
                mImg.setImageResource(R.drawable.depot_in_selected);

                for (BatchOutBean bean : beanList) {
                    bean.setSelect(true);
                }
            }
            if (adapter != null) {
                adapter.notifyData(beanList);
            }

        });

        mDepot.setOnClickListener(v -> {
            wasteIDs.clear();
            wasteTypes.clear();
            JSONArray dtos = new JSONArray();
            Set<BatchOutBean> newList = Paper.book().read(Constants.NEW_BATCH_OUT_LIST);
            if (newList != null && newList.size() > 0) {
                for (BatchOutBean outBean : newList) {
                    if (outBean.isSelect()) {
                        JSONObject object = new JSONObject();
                        object.put("id", outBean.getId());
                        object.put("weight", outBean.getWeight());
                        dtos.add(object);

                        wasteIDs.add(outBean.getId());
                        wasteTypes.add(outBean.getWasteClassifyCode());
                    }
                }
                Paper.book().write(Constants.WASTE_BASE_IDS, wasteIDs);
            }
            if (wasteIDs.size() > 0) {
                if (StringUtil.removeDuplicate(wasteTypes).size() > 1) {
                    Toast.makeText(this, "只能选择同种类型的医废！", Toast.LENGTH_SHORT).show();
                } else {
                    Paper.book().delete(Constants.NEW_BATCH_OUT_LIST);
                    Intent intent = new Intent(BatchOutActivity.this, BatchOutCheckActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else {
                Toast.makeText(BatchOutActivity.this, "请至少选中一项！", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BatchTypeDialog.mInstance = null;
    }
}