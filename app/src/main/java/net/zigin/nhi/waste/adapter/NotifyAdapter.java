package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.bean.NotifyBean;
import net.zigin.nhi.waste.bean.PrintMsgBean;
import net.zigin.nhi.waste.view.notify.NotifyDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class NotifyAdapter extends RecyclerView.Adapter<NotifyAdapter.ViewHolder>{

    public List<NotifyBean> list;

    public NotifyAdapter(List<NotifyBean> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<NotifyBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notify, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotifyBean bean = list.get(position);
        holder.mTitle.setText(bean.getTitle());
        holder.mContent.setText(bean.getContent());
        holder.mName.setText(bean.getCreateUser());
        holder.mTime.setText(bean.getCreateTime());
        holder.mDepartName.setText(bean.getHospitalDepartName());

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), NotifyDetailActivity.class);
            intent.putExtra("notifyID", bean.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mContent;
        private TextView mDepartName;
        private TextView mName;
        private TextView mTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title);
            mContent = itemView.findViewById(R.id.content);
            mDepartName = itemView.findViewById(R.id.departName);
            mName = itemView.findViewById(R.id.name);
            mTime = itemView.findViewById(R.id.time);
        }
    }
}
