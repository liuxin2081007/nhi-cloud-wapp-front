package net.zigin.nhi.waste.view.depotout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.PrintMsgAdapter;
import net.zigin.nhi.waste.bean.PrintMsgBean;
import net.zigin.nhi.waste.bean.ReceiverBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.NewPrintDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Printer.PrintHelper;
import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 医废出库打印界面
 */
public class OutPrintActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout mAddLayout;
    private RelativeLayout mReceiverLayout;
    private RecyclerView mRvMsg;
    private TextView mTotal;
    private TextView mOuter;
    private Button mBtnPrint;
    private Button mBtnPrintCount;
    private TextView mCarNumber;
    private TextView mName;
    private TextView mPhone;
    private WebView mWebView;

    private ReceiverBean bean;
    private PrintMsgAdapter adapter;
    private String mode;    //上级出库界面传进来的出库方式
    private String origin;  //上级出库核对界面传进来的来源是医废或者箱
    private int printCount = 1; // 打印份数

    private PrintHelper printHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_print);
        initView();

        printHelper = new PrintHelper();
        printHelper.Open(OutPrintActivity.this);
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mAddLayout = findViewById(R.id.add_layout);
        mReceiverLayout = findViewById(R.id.receiver_layout);
        mRvMsg = findViewById(R.id.rv_msg);
        mTotal = findViewById(R.id.total);
        mOuter = findViewById(R.id.outer);
        mBtnPrint = findViewById(R.id.btn_print);
        mBtnPrintCount = findViewById(R.id.btn_print_count);
        mCarNumber = findViewById(R.id.car_number);
        mName = findViewById(R.id.name);
        mPhone = findViewById(R.id.phone);
        mWebView = findViewById(R.id.web_view);

        mAddLayout.setOnClickListener(this);
        mReceiverLayout.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mBtnPrintCount.setOnClickListener(this);
        mRvMsg.setLayoutManager(new LinearLayoutManager(this));
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        mode = intent.getStringExtra("out_mode");
        origin = intent.getStringExtra("origin");

        bean = Paper.book().read(Constants.RECEIVER_BEAN);
        String realName = Paper.book().read(Constants.REALNAME);
        if (StringUtil.isNotEmpty(realName)) {
            mOuter.setText(realName);
        }

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new PrintObject(), "connect");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClientDemo());
        mWebView.loadUrl("file:///android_asset/output.html");

        getDefaultReceiver();
    }

    /**
     * 获取默认接收人
     */
    private void getDefaultReceiver() {
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getDefault(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("默认接收人", data);
                    JSONObject object = JSON.parseObject(data);
                    mCarNumber.setText(object.getString("carCode"));
                    mName.setText(object.getString("realName"));
                    mPhone.setText(object.getString("mobile"));

                    String receiverID = object.getString("id");
                    if (StringUtil.isNotEmpty(receiverID)) {
                        Paper.book().write(Constants.RECEIVER_ID, receiverID);
                    }

                    mReceiverLayout.setVisibility(View.VISIBLE);
                    mAddLayout.setVisibility(View.GONE);

                    if (bean != null) {
                        mCarNumber.setText(bean.getCarCode());
                        mName.setText(bean.getRealName());
                        mPhone.setText(bean.getMobile());
                        Paper.book().write(Constants.RECEIVER_ID, bean.getId());
                    }
                } else {
                    mReceiverLayout.setVisibility(View.GONE);
                    mAddLayout.setVisibility(View.VISIBLE);

                    if (bean != null) {
                        mCarNumber.setText(bean.getCarCode());
                        mName.setText(bean.getRealName());
                        mPhone.setText(bean.getMobile());
                        Paper.book().write(Constants.RECEIVER_ID, bean.getId());
                        mReceiverLayout.setVisibility(View.VISIBLE);
                        mAddLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    /**
     * 获取打印信息
     */
    private void getPrintMessage() {
        List<String> wasteIdList = Paper.book().read(Constants.WASTE_BASE_IDS);
        Map<String, Object> map = new HashMap<>();
        if (mode.equals("box")) {
            map.put("wasteBoxRecordId", Paper.book().read(Constants.WASTE_BOX_RECORD_ID));
        } else {
            map.put("wasteBaseIds", wasteIdList);
        }
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getPrintMessage(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @SuppressLint("DefaultLocale")
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("获取打印信息", data);
                    JSONObject object = JSON.parseObject(data);
                    String ckCode = object.getString("ckCode");
                    Paper.book().write(Constants.CK_CODE, ckCode);

                    List<PrintMsgBean> printMsgList = JSONArray.parseArray(object.getString("list"), PrintMsgBean.class);
                    if (printMsgList != null && printMsgList.size() > 0) {
                        adapter = new PrintMsgAdapter(printMsgList);
                        mRvMsg.setAdapter(adapter);
                    }

                    switch (mode) {
                        case "scan":    //扫码
                            JSONArray dtos;
                            double totalScanWeight = 0.00;
                            switch (origin) {
                                case "scanOut":
                                    dtos = Paper.book().read(Constants.WASTE_SCAN_DTOS);
                                    for (int i = 0; i < dtos.size(); i++) {
                                        JSONObject o = (JSONObject) dtos.get(i);
                                        double weight = Double.parseDouble(o.getString("weight"));
                                        totalScanWeight = totalScanWeight + weight;
                                    }
                                    break;
                                case "batchItemOut":
                                    dtos = Paper.book().read(Constants.WASTE_BATCH_ITEM_DTOS);
                                    for (int i = 0; i < dtos.size(); i++) {
                                        JSONObject o = (JSONObject) dtos.get(i);
                                        double weight = Double.parseDouble(o.getString("weight"));
                                        totalScanWeight = totalScanWeight + weight;
                                    }
                                    break;
                                default:
                                    dtos = new JSONArray();
                            }
                            mTotal.setText(String.format("%.2f", totalScanWeight));
                            break;
                        case "batch":   //批量
                            String totalWeight = Paper.book().read(Constants.BATCH_WEIGHT);
                            mTotal.setText(totalWeight);
                            break;
                        case "box":     //箱
                            String totalBoxWeight = Paper.book().read(Constants.WASTE_OUT_WEIGHT);
                            mTotal.setText(totalBoxWeight);
                            break;
                        default:
                            break;
                    }

                    //给webView传递数据
                    Map<String, Object> jsonData = new HashMap<>();
                    jsonData.put("ckCode", ckCode);
                    jsonData.put("dataList", printMsgList);
                    String json = JSONObject.toJSONString(jsonData);
                    Log.i("printObject", json);
                    String call = "javascript:getDetail('" + json + "')";
                    mWebView.loadUrl(call);
                }
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_layout:       //添加接收人
            case R.id.receiver_layout:  //接收人信息
                Intent intent = new Intent(OutPrintActivity.this, ReceiverActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_print:        //立即打印
                if (mAddLayout.getVisibility() == View.VISIBLE) {
                    Toast.makeText(this, "请先选择接收人！", Toast.LENGTH_SHORT).show();
                } else {
                    if (StringUtil.isNotEmpty(origin)) {
                        Map<String, Object> map = new HashMap<>();
                        JSONArray dtos;
                        switch (origin) {
                            case "scanOut":
                                dtos = Paper.book().read(Constants.WASTE_SCAN_DTOS);
                                break;
                            case "batchItemOut":
                                dtos = Paper.book().read(Constants.WASTE_BATCH_ITEM_DTOS);
                                break;
                            default:
                                dtos = new JSONArray();
                        }

                        switch (mode) {
                            case "scan":
                                if (dtos != null && dtos.size() > 0) {
                                    map.put("wasteBaseDtos", dtos);
                                } else {
                                    Toast.makeText(OutPrintActivity.this, "未查询到出库信息！", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                break;
                            case "batch":
                                map.put("wasteBaseIds", Paper.book().read(Constants.BATCH_WASTE_IDS));
                                map.put("weight", Paper.book().read(Constants.BATCH_WEIGHT));
                                map.put("wasteClassifyCode", Paper.book().read(Constants.BATCH_TYPE));
                                String batchRemark = Paper.book().read(Constants.BATCH_REMARK);
                                if (StringUtil.isNotEmpty(batchRemark)) {
                                    map.put("remark", batchRemark);
                                }
                                break;
                            case "box":
                                JSONObject dto = new JSONObject();
                                dto.put("wasteClassifyCode", Paper.book().read(Constants.WASTE_OUT_BOX_TYPE));
                                dto.put("id", Paper.book().read(Constants.WASTE_OUT_BOX_ID));
                                dto.put("wasteBoxRecordId", Paper.book().read(Constants.WASTE_BOX_RECORD_ID));
                                dto.put("weight", Paper.book().read(Constants.WASTE_OUT_WEIGHT));
                                map.put("wasteBoxDto", dto);
                                String boxRemark = Paper.book().read(Constants.WASTE_OUT_REMARK);
                                if (StringUtil.isNotEmpty(boxRemark)) {
                                    map.put("remark", boxRemark);
                                }
                                break;
                            default:
                                break;
                        }


                        map.put("revicerId", Paper.book().read(Constants.RECEIVER_ID));
                        map.put("ckCode", Paper.book().read(Constants.CK_CODE));

                        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                                .printMessage(Paper.book().read(Constants.TOKEN), map);
                        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                            @Override
                            protected void success(String data) {
                                Toast.makeText(OutPrintActivity.this, "出库成功！", Toast.LENGTH_SHORT).show();
                                //根据打印次数循环调用打印方法
                                int count = printCount;
                                Log.e("OutPrintActivity", "打印份数：" + count);
                                while (count > 0) {
                                    mWebView.loadUrl("javascript:scanprint()");
                                    count--;
                                }
                                Paper.book().delete(Constants.WASTE_SCAN_DTOS);
                                Paper.book().delete(Constants.WASTE_BATCH_ITEM_DTOS);
                                finish();
                            }

                            @Override
                            public void onError(Throwable e) {
                                super.onError(e);
                                Paper.book().delete(Constants.WASTE_SCAN_DTOS);
                                Paper.book().delete(Constants.WASTE_BATCH_ITEM_DTOS);
                                finish();
                            }
                        });
                    }

                }
                break;

            case R.id.btn_print_count:
                if (mAddLayout.getVisibility() == View.VISIBLE) {
                    Toast.makeText(this, "请先选择接收人", Toast.LENGTH_SHORT).show();
                } else {
                    new NewPrintDialog(this, count -> {
                        if (count < 1) {
                            Toast.makeText(this, "至少打印一份!", Toast.LENGTH_SHORT).show();
                        } else {
                            printCount = count;
                        }
                    }).show();
                }
                break;
            default:
                break;
        }
    }


    class PrintObject {
        @JavascriptInterface
        public void print(final int x, final int y, final int width, final int height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Picture pic = mWebView.capturePicture();
                        int nw = pic.getWidth();
                        int nh = pic.getHeight();
                        Bitmap bitmap = Bitmap.createBitmap(nw, nh, Bitmap.Config.ARGB_4444);
                        Canvas can = new Canvas(bitmap);
                        pic.draw(can);

                        int newWidth = nw;
                        int newHeight = nh;
                        if (nw > 400) {
                            float rate = 380 * 1.0f / nw * 1.0f;
                            newWidth = 380;
                            newHeight = (int) (nh * rate);
                        }
                        bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
                        Bitmap newBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
                        printHelper.PrintBitmap(newBitmap);

                        printHelper.printBlankLine(40);
                    } catch (Exception | Error e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @JavascriptInterface
        public void doFeed() {
            printHelper.Step((byte) 0x5f);
        }
    }

    private class WebViewClientDemo extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);// 当打开新链接时，使用当前的 WebView，不会使用系统其他浏览器
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //在这里执行你想调用的js函数
            getPrintMessage();
        }
    }
}