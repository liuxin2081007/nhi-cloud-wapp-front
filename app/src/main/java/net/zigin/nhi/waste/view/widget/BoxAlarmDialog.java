package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BoxAlarmAdapter;
import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.bean.AlarmBoxBean;

import java.util.List;

public class BoxAlarmDialog extends Dialog {

    private Activity activity;
    private RecyclerView mRvDetail;
    private List<AlarmBoxBean> baseList;
    private BoxAlarmAdapter adapter;

    public BoxAlarmDialog(@NonNull Context context, Activity activity, List<AlarmBoxBean> baseList) {
        super(context);
        this.activity = activity;
        this.baseList = baseList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_box_alarm);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mRvDetail = findViewById(R.id.rv_detail);

        Window window = getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);

        mRvDetail.setLayoutManager(new LinearLayoutManager(activity));
    }

    private void initData() {
        adapter = new BoxAlarmAdapter(baseList);
        mRvDetail.setAdapter(adapter);
    }
}
