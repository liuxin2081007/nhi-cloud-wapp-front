package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BoxLabelBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.utils.BitmapUtils;
import net.zigin.nhi.waste.view.print.EditBoxLabelActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.paperdb.Paper;

public class BoxLabelAdapter extends RecyclerView.Adapter<BoxLabelAdapter.ViewHolder>{

    public List<BoxLabelBean> list;
    private IActivityUpData dd;

    private boolean isItemCheck = false;

    private Set<BoxLabelBean> newList;


    public BoxLabelAdapter(List<BoxLabelBean> list, IActivityUpData dd) {
        this.list = list;
        this.dd = dd;
        newList = new HashSet<>();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<BoxLabelBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_box_label, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BoxLabelBean bean = list.get(position);
        holder.mNumber.setText(bean.getCode());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), EditBoxLabelActivity.class);
            intent.putExtra("boxId", bean.getId());
            v.getContext().startActivity(intent);
        });

        Bitmap bitmap = BitmapUtils.createQRImage(bean.getContent(), 500, 500);
        holder.mQrcode.setImageBitmap(bitmap);

        boolean isCheck = bean.isSelect();
        if (isCheck) {
            isItemCheck = true;
            holder.mImg.setImageResource(R.drawable.depot_in_selected);
        } else {
            isItemCheck = false;
            holder.mImg.setImageResource(R.drawable.depot_in_unselected);
        }

        newList.add(bean);
        Paper.book().write(Constants.NEW_BOX_LABEL_LIST, newList);

        holder.mImg.setOnClickListener(v -> {
            if (isItemCheck) {
                newList.remove(bean);
                isItemCheck = false;
                holder.mImg.setImageResource(R.drawable.depot_in_unselected);
                bean.setSelect(false);
                dd.upDataUi();

                newList.add(bean);
                Paper.book().write(Constants.NEW_BOX_LABEL_LIST, newList);
            } else {
                newList.remove(bean);
                isItemCheck = true;
                holder.mImg.setImageResource(R.drawable.depot_in_selected);
                bean.setSelect(true);

                newList.add(bean);
                Paper.book().write(Constants.NEW_BOX_LABEL_LIST, newList);
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImg;
        private TextView mNumber;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private ImageView mQrcode;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.img);
            mNumber = itemView.findViewById(R.id.number);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mQrcode = itemView.findViewById(R.id.qrcode);
        }
    }
}
