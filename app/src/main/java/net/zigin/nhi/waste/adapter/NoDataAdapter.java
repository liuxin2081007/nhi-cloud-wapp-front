package net.zigin.nhi.waste.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;


/**<Enter>
 * 无数据界面适配器
 */
public class NoDataAdapter extends RecyclerView.Adapter<NoDataAdapter.ViewHolder>{

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_no_data,parent,false);
        ViewHolder holder=new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.nodata.setText("未查询到数据！");
    }
    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nodata;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nodata = itemView.findViewById(R.id.tv_no_data);
        }

    }

}
