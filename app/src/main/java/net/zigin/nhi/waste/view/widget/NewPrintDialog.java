package net.zigin.nhi.waste.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import net.zigin.nhi.waste.R;

/**
 * @Author:
 * @Description:
 */
public class NewPrintDialog extends Dialog {

    private RelativeLayout mSub;
    private EditText mCount;
    private RelativeLayout mAdd;
    private Button mCancel;
    private Button mConfirm;

    private OnPrintCountListener mOnPrintCountListener;


    public NewPrintDialog(@NonNull Context context, OnPrintCountListener onPrintCountListener) {
        super(context);
        this.mOnPrintCountListener = onPrintCountListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_print);
        initView();
        initListener();
    }

    private void initView() {
        mSub = findViewById(R.id.sub);
        mCount = findViewById(R.id.count);
        mAdd = findViewById(R.id.add);
        mCancel = findViewById(R.id.cancel);
        mConfirm = findViewById(R.id.confirm);
    }

    private void initListener() {
        mSub.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            if (count > 1) {
                count--;
                mCount.setText(String.valueOf(count));
            } else {
                Toast.makeText(getContext(), "至少打印一份！", Toast.LENGTH_SHORT).show();
            }
        });

        mAdd.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            count++;
            mCount.setText(String.valueOf(count));
        });

        mCancel.setOnClickListener(v -> {
            dismiss();
        });

        mConfirm.setOnClickListener(v -> {
            dismiss();

            String count = mCount.getText().toString().trim();

            if (mOnPrintCountListener != null) {
                mOnPrintCountListener.onPrintCount(Integer.parseInt(count));
            }
        });
    }


    public interface OnPrintCountListener {
        /**
         * 监听打印份数
         *
         * @param count
         */
        void onPrintCount(int count);
    }
}
