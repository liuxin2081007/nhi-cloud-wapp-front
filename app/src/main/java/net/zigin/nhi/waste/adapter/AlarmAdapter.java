package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.alarm.AlarmBoxActivity;
import net.zigin.nhi.waste.view.alarm.AlarmCollectActivity;
import net.zigin.nhi.waste.view.alarm.AlarmTimeActivity;
import net.zigin.nhi.waste.view.alarm.AlarmWeightActivity;

import java.util.ArrayList;
import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.ViewHolder>{

    public List<AlarmBean> list;

    public AlarmAdapter(List<AlarmBean> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<AlarmBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alarm, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AlarmBean bean = list.get(position);
        if (StringUtil.isNotEmpty(bean.getObjectType())) {
            switch (bean.getObjectType()) {
                case "box":
                    holder.mNumType.setText("医废箱编号：");
                    break;
                case "waste":
                    holder.mNumType.setText("医废编号：");
                    break;
                case "depart":
                    holder.mNumType.setText("科室编号：");
                    break;
            }
        }
        holder.mNumber.setText(bean.getObjectCode());
        holder.mTime.setText(bean.getCreateTime());
        switch (bean.getType()) {
            case "in_depot_weight_red":
            case "out_depot_weight_red":
                holder.mType.setText("超重");
                holder.mImgAlarm.setImageResource(R.drawable.red_weight);
                holder.mType.setTextColor(Color.parseColor("#F4664A"));
                break;
            case "in_depot_weight_yellow":
            case "out_depot_weight_yellow":
                holder.mType.setText("超重");
                holder.mImgAlarm.setImageResource(R.drawable.yellow_weight);
                holder.mType.setTextColor(Color.parseColor("#FFAA00"));
                break;
            case "out_depot_time_red":
            case "in_depot_time_red":
            case "do_collect_time_red":
                holder.mType.setText("超时");
                holder.mImgAlarm.setImageResource(R.drawable.red_time);
                holder.mType.setTextColor(Color.parseColor("#F4664A"));
                break;
            case "out_depot_time_yellow":
            case "in_depot_time_yellow":
            case "do_collect_time_yellow":
                holder.mType.setText("超时");
                holder.mImgAlarm.setImageResource(R.drawable.yellow_time);
                holder.mType.setTextColor(Color.parseColor("#FFAA00"));
                break;
        }

        holder.itemView.setOnClickListener(v -> {
            if (StringUtil.isNotEmpty(bean.getObjectType())) {
                switch (bean.getObjectType()) {
                    case "box":
                        Intent boxIntent = new Intent(v.getContext(), AlarmBoxActivity.class);
                        boxIntent.putExtra("id", bean.getId());
                        boxIntent.putExtra("objectCode", bean.getObjectCode());
                        boxIntent.putExtra("alarm_type", bean.getType());
                        boxIntent.putExtra("objectId", bean.getObjectId());
                        boxIntent.putExtra("modifyStatus", bean.getStatus());
                        boxIntent.putExtra("modifyRemark", bean.getRemark());
                        boxIntent.putExtra("modifyTime", bean.getModifyTime());
                        boxIntent.putExtra("modifyUser", bean.getModifyUser());
                        v.getContext().startActivity(boxIntent);
                        break;
                    case "waste":
                        if (holder.mType.getText().toString().equals("超重")) {
                            Intent intent = new Intent(v.getContext(), AlarmWeightActivity.class);
                            intent.putExtra("id", bean.getId());
                            intent.putExtra("wasteBaseId", bean.getWasteBaseId());
                            intent.putExtra("code", bean.getObjectCode());
                            intent.putExtra("waste_type", bean.getWasteClassifyId());
                            intent.putExtra("alarm_type", bean.getType());
                            intent.putExtra("hospitalPlaceName", bean.getHospitalPlaceName());
                            intent.putExtra("collectUserStaffName", bean.getCollectUserStaffName());
                            intent.putExtra("handUserStaffName", bean.getHandUserStaffName());
                            intent.putExtra("modifyStatus", bean.getStatus());
                            intent.putExtra("modifyRemark", bean.getRemark());
                            intent.putExtra("modifyTime", bean.getModifyTime());
                            intent.putExtra("modifyUser", bean.getModifyUser());
                            v.getContext().startActivity(intent);
                        } else if (holder.mType.getText().toString().equals("超时")) {
                            Intent intent = new Intent(v.getContext(), AlarmTimeActivity.class);
                            intent.putExtra("id", bean.getId());
                            intent.putExtra("wasteBaseId", bean.getWasteBaseId());
                            intent.putExtra("code", bean.getObjectCode());
                            intent.putExtra("waste_type", bean.getWasteClassifyId());
                            intent.putExtra("alarm_type", bean.getType());
                            intent.putExtra("hospitalPlaceName", bean.getHospitalPlaceName());
                            intent.putExtra("hospitalDepartName", bean.getHospitalDepartName());
                            intent.putExtra("collectUserStaffName", bean.getCollectUserStaffName());
                            intent.putExtra("handUserStaffName", bean.getHandUserStaffName());
                            intent.putExtra("modifyStatus", bean.getStatus());
                            intent.putExtra("modifyRemark", bean.getRemark());
                            intent.putExtra("modifyTime", bean.getModifyTime());
                            intent.putExtra("modifyUser", bean.getModifyUser());
                            v.getContext().startActivity(intent);
                        }
                        break;
                    case "depart":
                        Intent intent = new Intent(v.getContext(), AlarmCollectActivity.class);
                        intent.putExtra("createTime", bean.getCreateTime());
                        intent.putExtra("id", bean.getId());
                        intent.putExtra("time", bean.getTime());
                        intent.putExtra("alarm_type", bean.getType());
                        intent.putExtra("hospitalDepartName", bean.getHospitalDepartName());
                        intent.putExtra("modifyStatus", bean.getStatus());
                        intent.putExtra("modifyRemark", bean.getRemark());
                        intent.putExtra("modifyTime", bean.getModifyTime());
                        intent.putExtra("modifyUser", bean.getModifyUser());
                        v.getContext().startActivity(intent);
                        break;
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImgAlarm;
        private TextView mNumType;
        private TextView mNumber;
        private TextView mTime;
        private TextView mType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNumber = itemView.findViewById(R.id.number);
            mNumType = itemView.findViewById(R.id.num_type);
            mTime = itemView.findViewById(R.id.time);
            mType = itemView.findViewById(R.id.type);
            mImgAlarm = itemView.findViewById(R.id.img_alarm);

        }
    }
}
