package net.zigin.nhi.waste.view.depotin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.MainApplication;
import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.scan.CodeListener;
import net.zigin.nhi.waste.scan.CodeReceiver;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.collect.CollectPrintActivity;
import net.zigin.nhi.waste.view.collect.HandoverScanActivity;
import net.zigin.nhi.waste.view.collect.OfficeScanActivity;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 扫收集人码界面
 */
public class CollectorActivity extends AppCompatActivity implements CodeListener {

    private CodeReceiver receiver;
    private LinearLayout mScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collector);

        initView();
        initReceiver();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }

    private void initReceiver() {
        receiver = new CodeReceiver();
        receiver.setListener(this);
        registerReceiver(receiver, new IntentFilter("com.barcode.sendBroadcast"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (MainApplication.scanManager != null) {
                //开启扫描
                Intent intent = new Intent();
                intent.setAction("com.barcode.sendBroadcastScan");
                sendBroadcast(intent);
            } else {
                Intent intent = new Intent(CollectorActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 界面跳转
     * @param data
     */
    private void jump(String data) {
        if (StringUtil.isNotEmpty(data)) {
            Map<String, Object> typeMap = new HashMap<>();
            typeMap.put("content", data);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getQrCodeType(Paper.book().read(Constants.TOKEN), typeMap);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String type) {
                    if (StringUtil.isNotEmpty(type)) {
                        if ("userStaff".equals(type)) {
                            Paper.book().write(Constants.COLLECTOR_CODE, data);
                            Intent intent = new Intent(CollectorActivity.this, DepotInActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(CollectorActivity.this, "请扫收集人二维码！", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            });
        }
    }

    @Override
    public void getData(String data) {
        Log.i("扫码结果", data);
        jump(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                jump(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
            System.gc();
        }
        super.onDestroy();
    }
}