package net.zigin.nhi.waste.view.depotout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;
import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.adapter.ReceiverAdapter;
import net.zigin.nhi.waste.bean.ReceiverBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 接收人详情界面
 */
public class ReceiverActivity extends AppCompatActivity {

    private TitleBarLayout mTitle;
    private RecyclerView mRvReceiver;
    private Button mAdd;

    private List<ReceiverBean> receiverList = new ArrayList<>();
    private ReceiverAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private boolean isManage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        initView();
        initListener();
    }


    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mRvReceiver = findViewById(R.id.rv_receiver);
        mAdd = findViewById(R.id.add);
        mRvReceiver.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initListener() {
        if (Paper.book().exist("manage")) {
            Paper.book().delete("manage");
        }
        mAdd.setOnClickListener(v -> {
            Intent intent = new Intent(ReceiverActivity.this, AddReceiverActivity.class);
            startActivity(intent);
        });

        mTitle.setRightTextClickListener(v -> {
            if (isManage) {
                if (adapter != null && receiverList != null && receiverList.size() > 0) {
                    Paper.book().write("manage", "no");
                    adapter.notifyData(receiverList);
                    mTitle.setRightText("管理");
                    isManage = false;
                }
            } else {
                if (adapter != null && receiverList != null && receiverList.size() > 0) {
                    Paper.book().write("manage", "manage");
                    adapter.notifyData(receiverList);
                    mTitle.setRightText("完成");
                    isManage = true;
                } else {
                    Toast.makeText(ReceiverActivity.this, "暂无接收人信息，请添加！", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getReceiverList();
    }

    /**
     * 获取接收人列表
     */
    public void getReceiverList() {
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getReceiverList(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("接收人列表", data);
                    receiverList = JSONArray.parseArray(data, ReceiverBean.class);
                    if (receiverList != null && receiverList.size() > 0) {
                        if (adapter == null || mRvReceiver.getAdapter() == null || !(mRvReceiver.getAdapter() instanceof ReceiverAdapter)) {
                            adapter = new ReceiverAdapter(ReceiverActivity.this, receiverList);
                            mRvReceiver.setAdapter(adapter);

                            adapter.setOnItemClickListener((position, bean) -> {
                                Paper.book().write(Constants.RECEIVER_BEAN, bean);
                                finish();
                            });
                        } else {
                            adapter.notifyData(receiverList);
                        }
                    } else {
                        mRvReceiver.setAdapter(noDataAdapter);
                    }
                } else {
                    Paper.book().delete(Constants.RECEIVER_BEAN);
                    mRvReceiver.setAdapter(noDataAdapter);
                    Toast.makeText(ReceiverActivity.this, "暂无接收人信息，请添加！", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}