package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.IAnnounceUpData;
import net.zigin.nhi.waste.adapter.NotifyReceiverAdapter;
import net.zigin.nhi.waste.bean.CustomReceiverBean;
import net.zigin.nhi.waste.bean.NotifyReceiverBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class NotifyReceiverDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvReceiver;
    private Button mConfirm;
    private Activity activity;
    private IAnnounceUpData dd;

    private List<String> nameList = new ArrayList<>();
    private List<String> idList = new ArrayList<>();
    public static NotifyReceiverDialog mInstance;

    public NotifyReceiverDialog(@NonNull Context context, Activity activity, IAnnounceUpData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static NotifyReceiverDialog getInstance(Context context, Activity activity, IAnnounceUpData dd) {
        if (mInstance == null) {
            synchronized (NotifyReceiverDialog.class) {
                if (mInstance == null) {
                    mInstance = new NotifyReceiverDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_notify_receicer);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvReceiver = findViewById(R.id.rv_receiver);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvReceiver.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    private void initData() {
        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        getReceiver();

        mConfirm.setOnClickListener(v -> {
            dd.upDataUi(nameList, idList);
            dismiss();
        });
    }

    /**
     * 获取接收人列表
     */
    private void getReceiver() {
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getAllHospitalBaseStaff(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("公告接收人列表", data);
                    List<NotifyReceiverBean> list = JSONArray.parseArray(data, NotifyReceiverBean.class);

                    List<CustomReceiverBean> receiverBeanList = new ArrayList<>();
                    for (NotifyReceiverBean bean : list) {
                        CustomReceiverBean receiverBean = new CustomReceiverBean();
                        receiverBean.setReceiver(bean.getRealName());
                        receiverBean.setReceiverId(bean.getSysUserId());
                        receiverBeanList.add(receiverBean);
                    }

                    NotifyReceiverAdapter receiverAdapter = new NotifyReceiverAdapter(receiverBeanList);
                    mRvReceiver.setAdapter(receiverAdapter);
                    receiverAdapter.setOnItemClickListener(((position, bean) -> {
                        if (!bean.isSelect()) {
                            nameList.add(bean.getReceiver());
                            idList.add(bean.getReceiverId());
                            bean.setSelect(true);
                        } else {
                            for (String s : idList) {
                                if (s.equals(bean.getReceiverId())) {
                                    nameList.remove(bean.getReceiver());
                                    idList.remove(bean.getReceiverId());
                                }
                            }
                            bean.setSelect(false);
                        }
                        receiverAdapter.notifyDataSetChanged();
                    }));
                }
            }
        });
    }
}
