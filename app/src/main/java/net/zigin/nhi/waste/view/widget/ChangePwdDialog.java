package net.zigin.nhi.waste.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.MenuVoBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.main.LoginActivity;
import net.zigin.nhi.waste.view.main.MainActivity;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class ChangePwdDialog extends Dialog {

    private EditText mOldPassword;
    private EditText mPassword;
    private EditText mConfirmPwd;
    private Button mCancel;
    private Button mConfirm;

    public ChangePwdDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_password);
        initView();
        initData();
    }

    private void initView() {
        mOldPassword = findViewById(R.id.oldPassword);
        mPassword = findViewById(R.id.password);
        mConfirmPwd = findViewById(R.id.confirmPwd);
        mCancel = findViewById(R.id.cancel);
        mConfirm = findViewById(R.id.confirm);

        mCancel.setOnClickListener(v -> {
            dismiss();
        });
    }

    private void initData() {
        mConfirm.setOnClickListener(v -> {
            if (StringUtil.isEmpty(mOldPassword.getText().toString())) {
                Toast.makeText(getContext(), "请输入旧密码！", Toast.LENGTH_SHORT).show();
            } else if (StringUtil.isEmpty(mPassword.getText().toString())){
                Toast.makeText(getContext(), "请输入修改密码！", Toast.LENGTH_SHORT).show();
            } else if (StringUtil.isEmpty(mConfirmPwd.getText().toString())){
                Toast.makeText(getContext(), "请再次确认密码！", Toast.LENGTH_SHORT).show();
            } else if (!mPassword.getText().toString().equals(mConfirmPwd.getText().toString())) {
                Toast.makeText(getContext(), "两次输入的密码不一致！", Toast.LENGTH_SHORT).show();
            } else {
                String token = Paper.book().read(Constants.TOKEN);
                String userName = Paper.book().read(Constants.USERNAME);
                Map<String, Object> map = new HashMap<>();
                map.put("name", userName);
                map.put("password", mPassword.getText().toString());
                map.put("oldPassword", mOldPassword.getText().toString());
                if (token != null && userName != null) {
                    Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                            .modifyPassword(token, map);
                    HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                        @Override
                        protected void success(String data) {
                            Toast.makeText(getContext(), "修改密码成功！", Toast.LENGTH_SHORT).show();
                            dismiss();
                        }

                        @Override
                        public void error(String msg) {
                            super.error(msg);
                            Toast.makeText(getContext(), "修改密码失败，请重试！", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });
    }

}
