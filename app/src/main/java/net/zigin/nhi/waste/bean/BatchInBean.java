package net.zigin.nhi.waste.bean;

/**
 * 批量入库医废详情接口bean
 */
public class BatchInBean {
    private String code;
    private String createTime;
    private String createUser;
    private String handUserStaffId;
    private String handUserStaffName;
    private String hospitalBaseId;
    private String hospitalBaseName;
    private String hospitalDepartId;
    private String hospitalDepartName;
    private String id;
    private String modifyTime;
    private String remark;
    private String status;
    private String wasteClassifyCode;
    private String wasteClassifyName;
    private String weight;

    private boolean isSelect;  //是否全选

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHandUserStaffName() {
        return handUserStaffName;
    }

    public void setHandUserStaffName(String handUserStaffName) {
        this.handUserStaffName = handUserStaffName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getHandUserStaffId() {
        return handUserStaffId;
    }

    public void setHandUserStaffId(String handUserStaffId) {
        this.handUserStaffId = handUserStaffId;
    }

    public String getHospitalBaseId() {
        return hospitalBaseId;
    }

    public void setHospitalBaseId(String hospitalBaseId) {
        this.hospitalBaseId = hospitalBaseId;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartId() {
        return hospitalDepartId;
    }

    public void setHospitalDepartId(String hospitalDepartId) {
        this.hospitalDepartId = hospitalDepartId;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
