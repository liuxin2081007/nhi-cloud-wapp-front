package net.zigin.nhi.waste.view.depotin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BoxBindAdapter;
import net.zigin.nhi.waste.adapter.IActivityUpData;
import net.zigin.nhi.waste.bean.BoxWasteBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.AddBindDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 箱袋绑定界面
 */
public class BoxBindActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mScanBind;
    private LinearLayout mPackageBind;
    private TextView mBoxNum;
    private TextView mHospitalName;
    private TextView mWeight;
    private LinearLayout mAddBindLayout;
    private LinearLayout mBindLayout;
    private RecyclerView mBoxRv;
    private Button mBtnComplete;
    private Button mBtnAddBind;
    private LinearLayout mBtnLayout;

    private List<BoxWasteBean> wasteBaseList = new ArrayList<>();
    private BoxBindAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_bind);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScanBind = findViewById(R.id.scan_bind);
        mPackageBind = findViewById(R.id.package_bind);
        mBoxNum = findViewById(R.id.box_num);
        mHospitalName = findViewById(R.id.hospitalName);
        mWeight = findViewById(R.id.weight);
        mAddBindLayout = findViewById(R.id.add_bind_layout);
        mBindLayout = findViewById(R.id.bind_layout);
        mBoxRv = findViewById(R.id.box_rv);
        mBtnComplete = findViewById(R.id.btnComplete);
        mBtnAddBind = findViewById(R.id.btnAddBind);
        mBtnLayout = findViewById(R.id.btn_layout);

        mScanBind.setOnClickListener(this);
        mPackageBind.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
        mBtnAddBind.setOnClickListener(this);

        mBoxRv.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBoxWasteDetail();
    }

    /**
     * 解除绑定后通知此界面刷新
     */
    private IActivityUpData dd = new IActivityUpData() {
        @Override
        public void upDataUi() {
            getBoxWasteDetail();
        }
    };

    /**
     * 获取医废箱详情
     */
    private void getBoxWasteDetail() {
        Map<String, Object> map = new HashMap<>();
        map.put("collectUserQrCode", Paper.book().read(Constants.COLLECTOR_CODE));
        map.put("wasteBoxId", Paper.book().read(Constants.BOX_ID));
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getBoxInStorageInfo(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @SuppressLint("DefaultLocale")
            @Override
            protected void success(String data) {
                Log.i("箱袋绑定查详情", data);
                wasteBaseList.clear();
                JSONObject object = JSON.parseObject(data);
                JSONObject wasteBox = object.getJSONObject("wasteBox");
                String collectorID = wasteBox.getString("collectUserStaffId");
                if (StringUtil.isNotEmpty(collectorID)) {
                    Paper.book().write(Constants.COLLECTOR_ID, collectorID);
                }
                mBoxNum.setText(wasteBox.getString("code"));
                mHospitalName.setText(wasteBox.getString("hospitalBaseName"));
                double weight = Double.parseDouble(wasteBox.getString("collectWeight"));
                mWeight.setText(String.format("%.2f", weight));

                wasteBaseList = JSONArray.parseArray(object.getString("wasteBaseList"), BoxWasteBean.class);
                if (wasteBaseList != null && wasteBaseList.size() > 0) {
                    mAddBindLayout.setVisibility(View.GONE);
                    mBindLayout.setVisibility(View.VISIBLE);
                    mBtnLayout.setVisibility(View.VISIBLE);

                    String wasteClassifyCode = wasteBaseList.get(0).getWasteClassifyCode();
                    Paper.book().write(Constants.BOX_BIND_WASTE_TYPE, wasteClassifyCode);
                    adapter = new BoxBindAdapter(wasteBaseList, dd);
                    mBoxRv.setAdapter(adapter);
                } else {
                    Paper.book().delete(Constants.BOX_BIND_WASTE_TYPE);
                    mAddBindLayout.setVisibility(View.VISIBLE);
                    mBindLayout.setVisibility(View.GONE);
                    mBtnLayout.setVisibility(View.GONE);
                }
            }
        });
    }



    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_bind:    //扫码绑定
                Intent intent1 = new Intent();
                intent1.setClass(BoxBindActivity.this, ScanWasteActivity.class);
                intent1.putExtra("bind", "bind");
                startActivity(intent1);
                finish();
                break;
            case R.id.package_bind: //批量绑定
                Intent intent2 = new Intent(BoxBindActivity.this, BatchBindActivity.class);
                startActivity(intent2);
                break;
            case R.id.btnComplete:  //完成入库
                Intent intent3 = new Intent(BoxBindActivity.this, BoxBindCheckActivity.class);
                startActivity(intent3);
                finish();
                break;
            case R.id.btnAddBind:   //添加绑定
                AddBindDialog dialog = new AddBindDialog(BoxBindActivity.this, BoxBindActivity.this);
                dialog.show();
                break;
        }
    }
}