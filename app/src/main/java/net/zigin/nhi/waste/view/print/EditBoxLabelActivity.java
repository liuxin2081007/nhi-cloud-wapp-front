package net.zigin.nhi.waste.view.print;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;
import net.zigin.nhi.waste.view.widget.OfficeDialog;
import net.zigin.nhi.waste.view.widget.WasteTypeDialog;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class EditBoxLabelActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText mNumber;
    private EditText mWeight;
    private TextView mUnit;
    private ImageView mScan;
    private TextView mHospitalName;
    private ImageView mArrow;
    private ImageView mArrowType;
    private EditText mRemark;
    private Button mSave;
    private RelativeLayout mDepartNameLayout;
    private EditText mDepartName;
    private RelativeLayout mTypeLayout;
    private EditText mType;

    private String departID = "";
    private String boxId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_box_label);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mNumber = findViewById(R.id.number);
        mWeight = findViewById(R.id.weight);
        mUnit = findViewById(R.id.unit);
        mScan = findViewById(R.id.scan);
        mHospitalName = findViewById(R.id.hospitalName);
        mArrow = findViewById(R.id.arrow);
        mArrowType = findViewById(R.id.arrow_type);
        mRemark = findViewById(R.id.remark);
        mSave = findViewById(R.id.save);
        mDepartNameLayout = findViewById(R.id.departName_layout);
        mDepartName = findViewById(R.id.departName);
        mTypeLayout = findViewById(R.id.type_layout);
        mType = findViewById(R.id.type);

        mScan.setOnClickListener(this);
        mDepartNameLayout.setOnClickListener(this);
        mTypeLayout.setOnClickListener(this);
        mSave.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        boxId = intent.getStringExtra("boxId");
        getBoxInfo();
    }

    /**
     * 获取箱信息
     */
    @SuppressLint("SetTextI18n")
    private void getBoxInfo() {
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getBoxInfo(Paper.book().read(Constants.TOKEN), boxId);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String result) {
                if (StringUtil.isNotEmpty(result)) {
                    Log.i("集装箱标签详情", result);
                    JSONObject object = JSON.parseObject(result);
                    mNumber.setText(object.getString("code"));
                    mWeight.setText(object.getString("collectWeight"));
                    mHospitalName.setText(object.getString("hospitalBaseName"));
                    mDepartName.setText(object.getString("hospitalDepartName"));
                    mType.setText(object.getString("wasteClassifyName") + "医废");

                    departID = object.getString("hospitalDepartId");
                }
            }
        });
    }

    private IDialogUpData dd = new IDialogUpData() {
        @Override
        public void upDataUi(String data, String id) {
            if (StringUtil.isNotEmpty(data)) {
                mDepartName.setText(data);
                departID = id;
            }
        }
    };

    private IDialogUpData typeDD = new IDialogUpData() {
        @Override
        public void upDataUi(String data, String id) {
            if (StringUtil.isNotEmpty(data)) {
                mType.setText(data);
            }
        }
    };

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan:     //科室信息扫码
                break;
            case R.id.departName_layout: //选择科室
                OfficeDialog officeDialog = OfficeDialog.getInstance(this, EditBoxLabelActivity.this, dd);
                officeDialog.show();
                break;
            case R.id.type_layout:     //选择类型
                WasteTypeDialog wasteTypeDialog = WasteTypeDialog.getInstance(this, EditBoxLabelActivity.this, typeDD);
                wasteTypeDialog.show();
                break;
            case R.id.save:     //保存按钮
                if (StringUtil.isEmpty(mNumber.getText().toString()) || StringUtil.isEmpty(mWeight.getText().toString())
                || StringUtil.isEmpty(mHospitalName.getText().toString()) || StringUtil.isEmpty(mDepartName.getText().toString())
                || StringUtil.isEmpty(mType.getText().toString())) {
                    Toast.makeText(EditBoxLabelActivity.this, "请填写完整信息！", Toast.LENGTH_SHORT).show();
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", boxId);
                    switch (mType.getText().toString()) {
                        case "化学性医废":
                            map.put("wasteClassifyCode", "chemical");
                            break;
                        case "感染性医废":
                            map.put("wasteClassifyCode", "infection");
                            break;
                        case "损伤性医废":
                            map.put("wasteClassifyCode", "injury");
                            break;
                        case "病理性医废":
                            map.put("wasteClassifyCode", "pathology");
                            break;
                        case "药物性医废":
                            map.put("wasteClassifyCode", "drug");
                            break;
                    }
                    map.put("code", mNumber.getText().toString());
                    map.put("hospitalDepartId", departID);
                    map.put("weight", mWeight.getText().toString());
                    if (StringUtil.isNotEmpty(mRemark.getText().toString())) {
                        map.put("remark", mRemark.getText().toString());
                    }
                    Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                            .saveBoxLabel(Paper.book().read(Constants.TOKEN), map);
                    DialogManager.getInstance().showLoading(this);
                    HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                        @Override
                        protected void success(String data) {
                            Log.i("修改集装箱标签", data);
                            Toast.makeText(EditBoxLabelActivity.this, "修改成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OfficeDialog.mInstance = null;
        WasteTypeDialog.mInstance = null;
    }
}