package net.zigin.nhi.waste.adapter;

import java.util.List;

/**
 * 发布公告的接收人dialog通知activity刷新界面
 */
public interface IAnnounceUpData {
    void upDataUi(List<String> nameList, List<String> idList);
}
