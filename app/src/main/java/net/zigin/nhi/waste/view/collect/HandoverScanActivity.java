package net.zigin.nhi.waste.view.collect;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.MainApplication;
import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.scan.CodeListener;
import net.zigin.nhi.waste.scan.CodeReceiver;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;


/**
 * 医废收集移交人扫码界面
 */
public class HandoverScanActivity extends AppCompatActivity implements CodeListener {

    private CodeReceiver receiver;
    private LinearLayout mScan;
    private Map<String, Object> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_handover_scan);

        initView();
        initReceiver();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }

    private void initReceiver() {
        receiver = new CodeReceiver();
        receiver.setListener(this);
        registerReceiver(receiver, new IntentFilter("com.barcode.sendBroadcast"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (MainApplication.scanManager != null) {
                //开启扫描
                Intent intent = new Intent();
                intent.setAction("com.barcode.sendBroadcastScan");
                sendBroadcast(intent);
            } else {
                Intent intent = new Intent(HandoverScanActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 医废收集
     * @param data
     */
    private void collect(String data) {
        if(CommonUtils.isFastClick()){
            return;
        }
        if (StringUtil.isNotEmpty(data)) {
            map = Paper.book().read(Constants.COLLECTMAP);
            map.put("handUserQrCode", data);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .wasteCollect(Paper.book().read(Constants.TOKEN).toString(), map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("医废收集", data);
                    Paper.book().write(Constants.COLLECTID, data);
                    Intent intent = new Intent(HandoverScanActivity.this, CollectPrintActivity.class);
                    startActivity(intent);
                    finish();
                }

            });
        }
    }

    @Override
    public void getData(String data) {
        Log.i("扫码结果", data);
        collect(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                collect(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
            System.gc();
        }
        super.onDestroy();
    }
}