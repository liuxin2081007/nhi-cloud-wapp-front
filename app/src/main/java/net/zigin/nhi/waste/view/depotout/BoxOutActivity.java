package net.zigin.nhi.waste.view.depotout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BoxOutAdapter;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.BoxOutBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 集装箱出库界面
 */
public class BoxOutActivity extends AppCompatActivity {

    private SmartRefreshLayout mRefresh;
    private RecyclerView mRvBox;
    private BoxOutAdapter adapter;
    private List<BoxOutBean> beanList = new ArrayList<>();
    private NoDataAdapter noDataAdapter = new NoDataAdapter();

    private int page = 1;
    private int limit = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_out);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mRefresh = findViewById(R.id.refresh);
        mRvBox = findViewById(R.id.rv_box);
        mRvBox.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRefresh.autoRefresh();

        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                beanList.clear();
                page = 1;
                getBoxList();
            }
        });

        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                Map<String, Object> map = new HashMap<>();
                map.put("operation", "pendingOut");
                map.put("isPage", true);
                map.put("pageIndex", page);
                map.put("pageSize", limit);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        if (StringUtil.isNotEmpty(data)) {
                            Log.i("箱列表", data);
                            List<BoxOutBean> newList = JSONArray.parseArray(data, BoxOutBean.class);
                            if (newList != null && newList.size() > 0) {
                                beanList.addAll(newList);
                                adapter.notifyData(beanList);
                            }
                            if (beanList.size() < limit) {
                                mRefresh.finishLoadMoreWithNoMoreData();
                            } else {
                                mRefresh.finishLoadMore();
                                page++;
                            }
                        } else {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        }
                    }
                });
            }
        });
    }

    /**
     * 获取集装箱列表
     */
    private void getBoxList() {
        Map<String, Object> map = new HashMap<>();
        map.put("operation", "pendingOut");
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("箱列表", data);
                    beanList = JSONArray.parseArray(data, BoxOutBean.class);
                    if (beanList != null && beanList.size() > 0) {
                        adapter = new BoxOutAdapter(beanList);
                        mRvBox.setAdapter(adapter);
                        page++;
                    } else {
                        mRvBox.setAdapter(noDataAdapter);
                    }
                } else {
                    mRvBox.setAdapter(noDataAdapter);
                }
                mRefresh.finishRefresh();
            }

            @Override
            public void error(String msg) {
                super.error(msg);
                mRefresh.finishRefresh();
                mRvBox.setAdapter(noDataAdapter);
            }
        });
    }
}