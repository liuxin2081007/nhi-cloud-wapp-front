package net.zigin.nhi.waste.view.history;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.DepotHistoryAdapter;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.InOutBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 医废出入库界面
 */
public class DepotHistoryActivity extends AppCompatActivity {

    private TextView mAll;
    private TextView mIn;
    private TextView mOut;
    private RecyclerView mRvHistory;
    private SmartRefreshLayout mRefresh;

    private DepotHistoryAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private Map<String, Object> map = new HashMap<>();
    private List<InOutBean> beanList = new ArrayList<>();

    private int page = 1;
    private int limit = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depot_history);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mAll = findViewById(R.id.all);
        mIn = findViewById(R.id.in);
        mOut = findViewById(R.id.out);
        mRvHistory = findViewById(R.id.rv_history);
        mRefresh = findViewById(R.id.refresh);

        mRvHistory.setLayoutManager(new LinearLayoutManager(this));
        map.put("model", "0");
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
        mRefresh.autoRefresh();
    }

    private void initData() {
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                beanList.clear();
                page = 1;
                getHistory();
            }
        });

        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                map.put("isPage", true);
                map.put("pageIndex", page);
                map.put("pageSize", limit);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getInOrOutList(Paper.book().read(Constants.TOKEN), map);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        if (StringUtil.isNotEmpty(data)) {
                            Log.i("出入库记录", data);
                            List<InOutBean> newList = JSONArray.parseArray(data, InOutBean.class);
                            if (newList != null && newList.size() > 0) {
                                beanList.addAll(newList);
                                adapter.notifyData(beanList);
                            }
                            if (beanList.size() < limit) {
                                mRefresh.finishLoadMoreWithNoMoreData();
                            } else {
                                mRefresh.finishLoadMore();
                                page++;
                            }
                        } else {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        }
                    }
                });
            }
        });

        mAll.setOnClickListener(v -> {
            mAll.setTextColor(Color.parseColor("#34BAFF"));
            mAll.setTypeface(Typeface.DEFAULT_BOLD);
            mIn.setTextColor(Color.parseColor("#333333"));
            mIn.setTypeface(Typeface.DEFAULT);
            mOut.setTextColor(Color.parseColor("#333333"));
            mOut.setTypeface(Typeface.DEFAULT);
            map.put("model", "0");
            getHistory();
        });

        mIn.setOnClickListener(v -> {
            mAll.setTextColor(Color.parseColor("#333333"));
            mAll.setTypeface(Typeface.DEFAULT);
            mIn.setTextColor(Color.parseColor("#34BAFF"));
            mIn.setTypeface(Typeface.DEFAULT_BOLD);
            mOut.setTextColor(Color.parseColor("#333333"));
            mOut.setTypeface(Typeface.DEFAULT);
            map.put("model", "1");
            getHistory();
        });

        mOut.setOnClickListener(v -> {
            mAll.setTextColor(Color.parseColor("#333333"));
            mAll.setTypeface(Typeface.DEFAULT);
            mIn.setTextColor(Color.parseColor("#333333"));
            mIn.setTypeface(Typeface.DEFAULT);
            mOut.setTextColor(Color.parseColor("#34BAFF"));
            mOut.setTypeface(Typeface.DEFAULT_BOLD);
            map.put("model", "2");
            getHistory();
        });

    }

    /**
     * 获取医废出入库记录
     */
    private void getHistory() {
        page = 1;
        beanList.clear();
        map.put("isPage", true);
        map.put("pageIndex", page);
        map.put("pageSize", limit);
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getInOrOutList(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("出入库记录", data);
                    beanList = JSONArray.parseArray(data, InOutBean.class);
                    if (beanList != null && beanList.size() > 0) {
                        adapter = new DepotHistoryAdapter(beanList);
                        mRvHistory.setAdapter(adapter);
                        page++;
                    } else {
                        mRvHistory.setAdapter(noDataAdapter);
                    }
                } else {
                    mRvHistory.setAdapter(noDataAdapter);
                }
                mRefresh.finishRefresh();
            }

            @Override
            public void error(String msg) {
                super.error(msg);
                mRefresh.finishRefresh();
                mRvHistory.setAdapter(noDataAdapter);
            }
        });
    }

}