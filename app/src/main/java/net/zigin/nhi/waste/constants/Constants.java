package net.zigin.nhi.waste.constants;

import com.clj.fastble.data.BleDevice;

public class Constants {

    //服务端根路径
//    public static String baseUrl = "http://182.92.118.11:9004/wapp/"; //开发环境地址
    public static String baseUrl = "http://132.232.31.196:9004/wapp/"; //开发环境地址
//    public static String baseUrl = "http://118.190.103.237:9700/wapp/"; //开发环境地址
//    public static String baseUrl = "http://123.60.8.149/wapp/"; //测试环境地址

    //登录接口返回结果中的menuVoList，用于动态展示功能模块
    public static String MENULIST = "menuList";
    //登录获取到的人员id，用于传进获取个人信息接口
    public static String STAFFID = "staffId";
    //登录进的用户名
    public static String USERNAME = "userName";
    //用户真实姓名
    public static String REALNAME = "realName";
    //登录获取到的token
    public static String TOKEN = "token";
    //当前连接的蓝牙设备
    public static BleDevice BLEDEVICE;
    //蓝牙连接状态
    public static boolean BLE_CONNECT_STATE = false;
    //当前连接的蓝牙MAC地址集合
    public static String BLEMACS = "bleMacs";
    //低功耗蓝牙service uuid
    public static String UUID_SERVICE = "uuid_service";
    //低功耗蓝牙character uuid
    public static String UUID_CHARACTER = "uuid_character";
    //医废收集界面要提交的数据
    public static String COLLECTMAP = "collectMap";
    //收集界面提交完获取到的ID，用于传进下载二维码和查询医废详情界面
    public static String COLLECTID = "collectID";
    //个人信息接口保存的医院名称
    public static String HOSPITAL_NAME = "hospitalName";
    //收集人二维码
    public static String COLLECTOR_CODE = "collectorCode";
    //收集到的医废二维码
    public static String WASTE_CODE = "wasteCode";
    //医废入库接口所需收集人id
    public static String COLLECTOR_ID = "collectorID";
    //扫码入库接口所需医废id
    public static String WASTE_ID = "wasteID";
    //医废收集到的所有二维码列表
    public static String WASTE_QRCODES = "wasteQRCodes";
    //批量入库中所有选中状态的bean
    public static String NEW_BATCH_LIST = "newBatchList";
    //批量出库中所有选中状态的bean
    public static String NEW_BATCH_OUT_LIST = "newBatchOutList";
    //RFID批量出库中所有选中状态的bean
    public static String NEW_RFID_BATCH_LIST = "newRfidBatchList";
    //医废标签中所有选中状态的bean
    public static String NEW_WASTE_LABEL_LIST = "newWasteLabelList";
    //集装箱标签中所有选中状态的bean
    public static String NEW_BOX_LABEL_LIST = "newBoxLabelList";
    //箱袋绑定查医废详情用的箱ID
    public static String BOX_ID = "boxID";
    //添加绑定所需要的箱二维码
    public static String BOX_CODE = "boxCode";
    //选中的接收人
    public static String RECEIVER_BEAN = "receiverBean";
    //接收人ID
    public static String RECEIVER_ID = "receiverID";
    //医废id列表，出库打印信息所需
    public static String WASTE_BASE_IDS = "wasteBaseIDs";
    //扫码出库打印所需要的信息
    public static String WASTE_SCAN_DTOS = "wasteScanDtos";
    //批量出库之子项单独出库打印所需要的信息
    public static String WASTE_BATCH_ITEM_DTOS = "wasteBatchItemDtos";
    //批量入库核对重量所需医废id列表
    public static String IN_CHECK = "inCheck";
    //批量出库核对重量所需医废id列表
    public static String OUT_CHECK = "outCheck";
    //RFID扫描出的epc
    public static String EPCS = "epcs";
    //集装箱标签id
    public static String BOX_LABEL_IDS = "box_label_ids";
    //医废类型---医废追溯筛选用
    public static String WASTE_CLASSIFY_CODE = "wasteClassifyCode";
    //医废状态---医废追溯筛选用
    public static String WASTE_STATUS = "wasteStatus";
    //科室---医废追溯筛选用
    public static String HOSPITAL_DEPART_ID = "hospitalDepartId";
    //出库单号
    public static String CK_CODE = "ckCode";
    //告警类型
    public static String ALARM_TYPE = "alarmType";
    //批量出库时重新称重的重量
    public static String BATCH_WEIGHT = "batchWeight";
    //批量出库时的备注
    public static String BATCH_REMARK = "batchRemark";
    //批量出库时的医废类型
    public static String BATCH_TYPE = "batchType";
    //批量出库时的医废id集合
    public static String BATCH_WASTE_IDS = "batchWasteIds";
    //集装箱出库的wasteBoxRecordId
    public static String WASTE_BOX_RECORD_ID = "wasteBoxRecordId";
    //集装箱出库printMessage需要的传参
    public static String WASTE_OUT_BOX_ID = "wasteOutBoxId";
    public static String WASTE_OUT_WEIGHT = "wasteOutBoxWeight";
    public static String WASTE_OUT_BOX_TYPE = "wasteOutBoxType";
    public static String WASTE_OUT_REMARK = "wasteOutBoxRemark";

    //记录下箱袋绑定所绑定的医废类型，用于拦截不同类型的医废绑定
    public static String BOX_BIND_WASTE_TYPE = "boxBindWasteType";

}
