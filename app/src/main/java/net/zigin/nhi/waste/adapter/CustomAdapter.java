package net.zigin.nhi.waste.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.MenuVoBean;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {

    private List<MenuVoBean> mList;
    private OnItemClickListener mOnItemClickListener;

    public CustomAdapter(List<MenuVoBean> mList) {
        this.mList = mList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_layout, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        MenuVoBean bean = mList.get(position);
        switch (bean.getUrl()) {
            case "depot_out"://医废出库
                holder.img.setImageResource(R.drawable.waste_output);
                holder.tv.setText(bean.getName());
                break;
            case "trace_record"://医废追溯
                holder.img.setImageResource(R.drawable.waste_history);
                holder.tv.setText(bean.getName());
                break;
            case "collect_collect"://医废收集
                holder.img.setImageResource(R.drawable.waste_collect);
                holder.tv.setText(bean.getName());
                break;
            case "depot_in"://医废入库
                holder.img.setImageResource(R.drawable.waste_input);
                holder.tv.setText(bean.getName());
                break;
            default:
                holder.itemView.setVisibility(View.GONE);
                break;
        }
        //设置点击事件
        setUIEvent(holder, bean);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, String url);
    }

    static class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView img;
        private TextView tv;

        public CustomViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            tv = itemView.findViewById(R.id.tv);
        }
    }

    /**
     * 设置子项点击事件
     * @param holder
     * @param bean
     */
    public void setUIEvent(final CustomViewHolder holder, MenuVoBean bean) {
        //点击事件，首先判断是否为空，不为空的时候再进行操作
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(holder.itemView, bean.getUrl());
                }
            });
        }
    }

    /**
     * 提供给Activity或者Fragment调用的
     *
     * @param onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }
}