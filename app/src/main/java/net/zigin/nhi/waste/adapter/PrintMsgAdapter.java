package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.PrintMsgBean;
import java.util.ArrayList;
import java.util.List;

public class PrintMsgAdapter extends RecyclerView.Adapter<PrintMsgAdapter.ViewHolder>{

    public List<PrintMsgBean> list;

    public PrintMsgAdapter(List<PrintMsgBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_print_msg, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PrintMsgBean bean = list.get(position);
        holder.mNumber.setText(bean.getCode());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mWeight.setText(bean.getWeight());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mNumber;
        private TextView mType;
        private TextView mWeight;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNumber = itemView.findViewById(R.id.number);
            mType = itemView.findViewById(R.id.type);
            mWeight = itemView.findViewById(R.id.weight);
        }
    }
}
