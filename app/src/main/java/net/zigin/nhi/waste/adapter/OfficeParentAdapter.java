package net.zigin.nhi.waste.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;

import java.util.List;

public class OfficeParentAdapter extends RecyclerView.Adapter<OfficeParentAdapter.ViewHolder>{

    public List<String> list;
    private int mPosition = -1;

    public OfficeParentAdapter(List<String> list) {
        this.list = list;
    }

    public interface GetListener {
        void onClick(int position, String text);
    }

    private GetListener getListener;

    public void setGetListener(GetListener getListener) {
        this.getListener = getListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent_office, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String text = list.get(position);
        holder.mParent.setText(text);

        holder.mLayout.setOnClickListener(v -> {
            getListener.onClick(position, text);
            notifyDataSetChanged();
        });

        if (position == getPosition()) {
            holder.mSelected.setBackgroundResource(R.drawable.bg_type_filter_selected);
            holder.mParent.setTextColor(Color.parseColor("#34BAFF"));
        } else {
            holder.mSelected.setBackgroundResource(R.drawable.bg_type_filter);
            holder.mParent.setTextColor(Color.parseColor("#333333"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private TextView mParent;
        private View mSelected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mParent = itemView.findViewById(R.id.parent);
            mSelected = itemView.findViewById(R.id.selected);
        }
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }
}
