package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotin.ScanWasteActivity;
import net.zigin.nhi.waste.view.depotout.OutPrintActivity;

import java.io.Serializable;
import java.util.List;

import io.paperdb.Paper;

public class AlarmOutDialog extends Dialog {

    private Button mForce;
    private Button mCancel;

    private Activity activity;
    private String origin;
    private String type;
    private String weight;
    private String remark;
    private String wasteClassifyCode;
    private String wasteBoxRecordId;
    private String id;
    private List<String> wasteIdList;

    public AlarmOutDialog(@NonNull Context context, Activity activity, String origin, String type, String weight, String remark, List<String> wasteIdList) {
        super(context);
        this.activity = activity;
        this.origin = origin;
        this.type = type;
        this.weight = weight;
        this.remark = remark;
        this.wasteIdList = wasteIdList;
    }

    public AlarmOutDialog(@NonNull Context context, Activity activity, String origin, String type, String weight, String remark, String wasteClassifyCode, String wasteBoxRecordId, String id) {
        super(context);
        this.activity = activity;
        this.origin = origin;
        this.type = type;
        this.weight = weight;
        this.remark = remark;
        this.wasteClassifyCode = wasteClassifyCode;
        this.wasteBoxRecordId = wasteBoxRecordId;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_out_abnormal);
        initView();
        initListener();
    }

    private void initView() {
        mForce = findViewById(R.id.force);
        mCancel = findViewById(R.id.cancel);
    }

    private void initListener() {
        mForce.setOnClickListener(v -> {    //强制出库
            if (type.equals("complete")) {  //完成出库
                switch (origin) {
                    case "batchItemOut":        //批量出库子项强制出库告警
                        saveBatchItemDtos();
                        Intent itemIntent = new Intent(v.getContext(), OutPrintActivity.class);
                        itemIntent.putExtra("origin", "batchItemOut");
                        //传出库方式给打印界面，printMessage根据出库方式决定传参
                        itemIntent.putExtra("out_mode", "scan");
                        v.getContext().startActivity(itemIntent);
                        activity.finish();
                        break;
                    case "scanOut":             //扫码出库强制出库告警
                        saveScanDtos();
                        Intent intent = new Intent(v.getContext(), OutPrintActivity.class);
                        intent.putExtra("origin", "scanOut");
                        //传出库方式给打印界面，printMessage根据出库方式决定传参
                        intent.putExtra("out_mode", "scan");
                        v.getContext().startActivity(intent);
                        activity.finish();
                        break;
                    case "boxOut":              //箱出库强制出库告警
                        Intent intent1 = new Intent(v.getContext(), OutPrintActivity.class);
                        intent1.putExtra("origin", "boxOut");
                        //传出库方式给打印界面，printMessage根据出库方式决定传参
                        intent1.putExtra("out_mode", "box");
                        Paper.book().write(Constants.WASTE_OUT_WEIGHT, weight);
                        Paper.book().write(Constants.WASTE_OUT_BOX_TYPE, wasteClassifyCode);
                        Paper.book().write(Constants.WASTE_BOX_RECORD_ID, wasteBoxRecordId);
                        Paper.book().write(Constants.WASTE_OUT_BOX_ID, id);
                        v.getContext().startActivity(intent1);
                        activity.finish();
                }
                dismiss();
            } else if (type.equals("continue")) {   //继续扫描
                switch (origin) {
                    case "scanOut":
                        saveScanDtos();
                        Intent intent2 = new Intent(v.getContext(), ScanWasteActivity.class);
                        //将获取到的医废id列表再传回扫码界面
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("IDLIST", (Serializable) wasteIdList);
                        intent2.putExtra("BUNDLE", bundle);
                        intent2.putExtra("scan_out", "continue_scan_out");
                        v.getContext().startActivity(intent2);
                        activity.finish();
                        break;
                }
                dismiss();

            }
        });

        mCancel.setOnClickListener(v -> {   //重新核对
            dismiss();
        });
    }

    /**
     * 将批量出库之子项出库的医废id和获取到的重量暂存起来，用于最后出库
     */
    private void saveBatchItemDtos() {
        JSONObject dto = new JSONObject();
        dto.put("id", Paper.book().read(Constants.WASTE_ID));
        dto.put("weight", weight);

        if (StringUtil.isNotEmpty(remark)) {
            dto.put("remark", remark);
        }

        JSONArray dtos = new JSONArray();
        dtos.add(dto);
        Paper.book().write(Constants.WASTE_BATCH_ITEM_DTOS, dtos);
    }

    /**
     * 将扫码出库的医废id和获取到的重量暂存起来，用于最后出库
     */
    private void saveScanDtos() {
        JSONObject dto = new JSONObject();
        dto.put("id", Paper.book().read(Constants.WASTE_ID));
        dto.put("weight", weight);

        if (StringUtil.isNotEmpty(remark)) {
            dto.put("remark", remark);
        }

        JSONArray dtos;
        dtos = Paper.book().read(Constants.WASTE_SCAN_DTOS);
        if (dtos == null || dtos.size() <= 0) {
            dtos = new JSONArray();
        }
        dtos.add(dto);
        Paper.book().write(Constants.WASTE_SCAN_DTOS, dtos);
    }

}
