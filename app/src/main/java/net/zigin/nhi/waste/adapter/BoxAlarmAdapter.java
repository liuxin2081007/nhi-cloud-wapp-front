package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.AlarmBoxBean;
import net.zigin.nhi.waste.bean.NotifyBean;
import net.zigin.nhi.waste.view.notify.NotifyDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class BoxAlarmAdapter extends RecyclerView.Adapter<BoxAlarmAdapter.ViewHolder>{

    public List<AlarmBoxBean> list;

    public BoxAlarmAdapter(List<AlarmBoxBean> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<AlarmBoxBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_box_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AlarmBoxBean bean = list.get(position);
        holder.mNum.setText(bean.getCode());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mWeight.setText(bean.getWeight());
        if (position == list.size() - 1) {
            holder.mLine.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mNum;
        private TextView mType;
        private TextView mWeight;
        private View mLine;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNum = itemView.findViewById(R.id.num);
            mType = itemView.findViewById(R.id.type);
            mWeight = itemView.findViewById(R.id.weight);
            mLine = itemView.findViewById(R.id.line);
        }
    }
}
