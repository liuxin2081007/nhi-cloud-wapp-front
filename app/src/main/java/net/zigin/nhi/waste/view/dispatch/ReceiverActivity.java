package net.zigin.nhi.waste.view.dispatch;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;


/**
 * 接收人界面
 */
public class ReceiverActivity extends AppCompatActivity {

    private TitleBarLayout mTitle;
    private RecyclerView mRvReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        initView();
    }

    private void initView() {
        mTitle = findViewById(R.id.title);
        mRvReceiver = findViewById(R.id.rv_receiver);

        mTitle.setRightTextClickListener(v -> {
            Intent intent = new Intent(ReceiverActivity.this, AddReceiverActivity.class);
            startActivity(intent);
        });
    }
}