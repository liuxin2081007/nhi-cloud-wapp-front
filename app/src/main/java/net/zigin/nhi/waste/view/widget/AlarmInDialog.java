package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotin.BatchItemInActivity;
import net.zigin.nhi.waste.view.depotin.ScanInActivity;
import net.zigin.nhi.waste.view.depotin.ScanWasteActivity;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class AlarmInDialog extends Dialog {

    private Button mForce;
    private Button mCancel;

    private Activity activity;
    private String origin;
    private String type;
    private String weight;
    private String remark;

    public AlarmInDialog(@NonNull Context context, Activity activity, String origin, String type, String weight, String remark) {
        super(context);
        this.activity = activity;
        this.origin = origin;
        this.type = type;
        this.weight = weight;
        this.remark = remark;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_in_abnormal);
        initView();
        initListener();
    }

    private void initView() {
        mForce = findViewById(R.id.force);
        mCancel = findViewById(R.id.cancel);
    }

    private void initListener() {
        mForce.setOnClickListener(v -> {    //强制入库
            if (type.equals("complete")) {
                switch (origin) {
                    case "batchItemIn":     //批量入库子项暂存入库告警
                        Map<String, Object> map = new HashMap<>();
                        map.put("collectUserId", Paper.book().read(Constants.COLLECTOR_ID));
                        map.put("wasteBaseId", Paper.book().read(Constants.WASTE_ID));
                        map.put("weight", weight);
                        if (StringUtil.isNotEmpty(remark)) {
                            map.put("remark", remark);
                        }
                        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                                .putSingleInStorage(Paper.book().read(Constants.TOKEN), map);
                        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                            @Override
                            protected void success(String data) {
                                Toast.makeText(v.getContext(), "强制暂存入库成功！", Toast.LENGTH_SHORT).show();
                                activity.finish();
                            }
                        });
                        break;
                    case "scanIn":      //扫码入库告警
                        Map<String, Object> map1 = new HashMap<>();
                        map1.put("collectUserId", Paper.book().read(Constants.COLLECTOR_ID));
                        map1.put("wasteBaseId", Paper.book().read(Constants.WASTE_ID));
                        map1.put("weight", weight);
                        if (StringUtil.isNotEmpty(remark)) {
                            map1.put("remark", remark);
                        }
                        Observable<BaseResponse<String>> observable1 = HttpRetrofitClient.getInstance().create(PublicService.class)
                                .putSingleInStorage(Paper.book().read(Constants.TOKEN), map1);
                        HttpRetrofitClient.execute(observable1, new ApiCall<String>() {
                            @Override
                            protected void success(String data) {
                                Toast.makeText(v.getContext(), "强制入库成功！", Toast.LENGTH_SHORT).show();
                                activity.finish();
                            }
                        });
                        break;
                }
                dismiss();
            } else if (type.equals("continue")) {
                switch (origin) {
                    case "scanIn":
                        Map<String, Object> map = new HashMap<>();
                        map.put("collectUserId", Paper.book().read(Constants.COLLECTOR_ID));
                        map.put("wasteBaseId", Paper.book().read(Constants.WASTE_ID));
                        map.put("weight", weight);
                        if (StringUtil.isNotEmpty(remark)) {
                            map.put("remark", remark);
                        }
                        Observable<BaseResponse<String>> observable2 = HttpRetrofitClient.getInstance().create(PublicService.class)
                                .putSingleInStorage(Paper.book().read(Constants.TOKEN), map);
                        HttpRetrofitClient.execute(observable2, new ApiCall<String>() {
                            @Override
                            protected void success(String data) {
                                Toast.makeText(v.getContext(), "强制入库成功！", Toast.LENGTH_SHORT).show();
                                activity.finish();
                            }
                        });
                        break;
                }
                Intent intent = new Intent(v.getContext(), ScanWasteActivity.class);
                v.getContext().startActivity(intent);
                dismiss();
            }
        });

        mCancel.setOnClickListener(v -> {   //重新核对
            dismiss();
        });
    }
}
