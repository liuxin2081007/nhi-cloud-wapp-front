package net.zigin.nhi.waste.view.print;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BoxLabelAdapter;
import net.zigin.nhi.waste.adapter.IActivityUpData;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.BoxLabelBean;
import net.zigin.nhi.waste.bean.WastePrintBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Printer.PrintHelper;
import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 集装箱标签界面
 */
public class BoxLabelActivity extends AppCompatActivity {

    private TitleBarLayout mTitle;
    private SmartRefreshLayout mRefresh;
    private RecyclerView mRv;
    private ImageView mImg;
    private Button mPrint;
    private WebView mWebView;

    private BoxLabelAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private boolean isSelected = false;
    private List<BoxLabelBean> beanList = new ArrayList<>();

    private int page = 1;
    private int limit = 20;

    private PrintHelper printHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_label);
        initView();

        printHelper = new PrintHelper();
        printHelper.Open(BoxLabelActivity.this);
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mRefresh = findViewById(R.id.refresh);
        mRv = findViewById(R.id.rv);
        mImg = findViewById(R.id.img);
        mWebView = findViewById(R.id.web_view);
        mPrint = findViewById(R.id.print);

        mRv.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initData() {
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                beanList.clear();
                page = 1;
                getBoxLabel();
            }
        });

        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                HashSet<String> ids = new HashSet<>();
                Map<String, Object> map = new HashMap<>();
                map.put("wasteBoxIds", ids);
                map.put("orderBy", "time");
                map.put("isPage", true);
                map.put("pageIndex", page);
                map.put("pageSize", limit);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String result) {
                        if (StringUtil.isNotEmpty(result)) {
                            Log.i("集装箱标签信息", result);
                            List<BoxLabelBean> newList = JSONArray.parseArray(result, BoxLabelBean.class);
                            if (newList != null && newList.size() > 0) {
                                beanList.addAll(newList);
                                adapter.notifyData(beanList);
                            }
                            if (beanList.size() < limit) {
                                mRefresh.finishLoadMoreWithNoMoreData();
                            } else {
                                mRefresh.finishLoadMore();
                                page++;
                            }
                        } else {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        }
                    }
                });
            }
        });

        mTitle.setRightTextClickListener(v -> {
            Intent intent = new Intent(BoxLabelActivity.this, AddBoxLabelActivity.class);
            startActivity(intent);
        });

        mImg.setOnClickListener(v -> {
            if (isSelected) {
                isSelected = false;
                mImg.setImageResource(R.drawable.depot_in_unselected);

                for (BoxLabelBean bean : beanList) {
                    bean.setSelect(false);
                }
            } else {
                isSelected = true;
                mImg.setImageResource(R.drawable.depot_in_selected);

                for (BoxLabelBean bean : beanList) {
                    bean.setSelect(true);
                }
            }
            if (adapter != null) {
                adapter.notifyData(beanList);
            }
        });

        mPrint.setOnClickListener(v -> {
            mWebView.clearCache(true);
            Set<BoxLabelBean> webList = new HashSet<>();   //给webView传参的list
            Set<BoxLabelBean> newList = Paper.book().read(Constants.NEW_BOX_LABEL_LIST);
            if (newList != null && newList.size() > 0) {
                for (BoxLabelBean bean : newList) {
                    if (bean.isSelect()) {
                        //已选中的打印
                        webList.add(bean);
                    }
                }
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (BoxLabelBean bean : webList) {
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //给webView传递数据
                        String json = JSONObject.toJSONString(bean);
                        Log.i("printObject", json);
                        String call = "javascript:getLabel('" + json + "')";
                        runOnUiThread(() -> {
                            mWebView.loadUrl(call);
                        });
                    }
                }
            }).start();
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new PrintObject(), "connect");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl("file:///android_asset/boxlabel.html");

        initData();
        mRefresh.autoRefresh();
    }

    /**
     * 获取集装箱标签列表
     */
    private void getBoxLabel() {
        HashSet<String> ids = new HashSet<>();
        Map<String, Object> map = new HashMap<>();
        map.put("wasteBoxIds", ids);
        map.put("orderBy", "time");
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String result) {
                if (StringUtil.isNotEmpty(result)) {
                    Log.i("集装箱标签信息", result);
                    beanList = JSONArray.parseArray(result, BoxLabelBean.class);
                    if (beanList != null && beanList.size() > 0) {
                        adapter = new BoxLabelAdapter(beanList, dd);
                        mRv.setAdapter(adapter);
                        page++;
                    } else {
                        mRv.setAdapter(noDataAdapter);
                    }
                } else {
                    mRv.setAdapter(noDataAdapter);
                }
                mRefresh.finishRefresh();
            }

            @Override
            public void error(String msg) {
                super.error(msg);
                mRefresh.finishRefresh();
                mRv.setAdapter(noDataAdapter);
            }
        });

    }

    /**
     * 子项取消选中时，全选按钮变更状态为未选中
     */
    private IActivityUpData dd = new IActivityUpData() {
        @Override
        public void upDataUi() {
            isSelected = false;
            mImg.setImageResource(R.drawable.depot_in_unselected);
        }
    };

    class PrintObject {
        @JavascriptInterface
        public void print(final int x, final int y, final int width, final int height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Picture pic = mWebView.capturePicture();
                        int nw = pic.getWidth();
                        int nh = pic.getHeight();
                        Bitmap bitmap = Bitmap.createBitmap(nw, nh, Bitmap.Config.ARGB_4444);
                        Canvas can = new Canvas(bitmap);
                        pic.draw(can);

                        int newWidth = nw;
                        int newHeight = nh;
                        if (nw > 400) {
                            float rate = 380 * 1.0f / nw * 1.0f;
                            newWidth = 380;
                            newHeight = (int) (nh * rate);
                        }
                        bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
                        Bitmap newBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
                        printHelper.PrintBitmap(newBitmap);

                        printHelper.printBlankLine(40);
                    } catch (Exception | Error e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @JavascriptInterface
        public void doFeed() {
            printHelper.Step((byte) 0x5f);
        }
    }
}