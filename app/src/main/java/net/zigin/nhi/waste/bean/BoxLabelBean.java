package net.zigin.nhi.waste.bean;

public class BoxLabelBean {
    private String code;
    private String hospitalBaseName;
    private String hospitalDepartName;
    private String id;
    private String content;
    private String wasteClassifyName;
    private String weight;

    private boolean isSelect;  //是否全选

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
