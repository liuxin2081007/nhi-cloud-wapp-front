package net.zigin.nhi.waste.adapter;


/**
 * 发布公告的发布类型dialog通知activity刷新界面
 */
public interface IAnnounceTypeData {
    void upDataUi(String type);
}
