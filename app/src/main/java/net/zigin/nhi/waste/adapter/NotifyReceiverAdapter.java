package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.CustomReceiverBean;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NotifyReceiverAdapter extends RecyclerView.Adapter<NotifyReceiverAdapter.ViewHolder>{

    public List<CustomReceiverBean> list;

    public NotifyReceiverAdapter(List<CustomReceiverBean> list) {
        this.list = list;
    }

    public interface OnItemClickListener {
        void onClick(int position, CustomReceiverBean bean);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_type, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CustomReceiverBean bean = list.get(position);
        holder.mTv.setText(bean.getReceiver());

        if (bean.isSelect()) {
            holder.mLayout.setBackgroundResource(R.drawable.bg_type_filter_selected);
            holder.mTv.setTextColor(Color.parseColor("#34BAFF"));
        } else {
            holder.mLayout.setBackgroundResource(R.drawable.bg_type_filter);
            holder.mTv.setTextColor(Color.parseColor("#333333"));
        }

        holder.mLayout.setOnClickListener(v -> {
            listener.onClick(position, bean);
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout mLayout;
        private TextView mTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mTv = itemView.findViewById(R.id.tv);
        }
    }
}
