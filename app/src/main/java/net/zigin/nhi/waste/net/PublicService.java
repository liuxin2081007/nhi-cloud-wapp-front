package net.zigin.nhi.waste.net;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PublicService {

    //账号密码登录
    @POST("sysUser/login")
    Observable<BaseResponse<String>> login(@Body Map<String, Object> map);

    //扫码登录
    @POST("sysUser/loginWithQrCode")
    Observable<BaseResponse<String>> loginWithQrCode(@Body Map<String, Object> map);

    //根据id查询个人信息
    @GET("sysUser/getStaffInfoById/{id}")
    Observable<BaseResponse<String>> getStaffInfoById(@Header("Authorization") String token, @Path("id") String id);

    //修改密码
    @POST("sysUser/modifyPassword")
    Observable<BaseResponse<String>> modifyPassword(@Header("Authorization") String token, @Body Map<String, Object> map);

    //医废收集
    @POST("wasteBase/wasteCollect")
    Observable<BaseResponse<String>> wasteCollect(@Header("Authorization") String token, @Body Map<String, Object> map);

    @POST("wasteBase/getOrCodeType")
    Observable<BaseResponse<String>> getOrCodeType(@Header("Authorization") String token, @Body Map<String, Object> map);

    //下载二维码
    @GET("wasteBase/generateQrCode/{id}")
    Observable<BaseResponse<String>> generateQrCode(@Header("Authorization") String token, @Path("id") String id);

    //查询医废详情
    @GET("wasteBase/getById/{id}")
    Observable<BaseResponse<String>> getById(@Header("Authorization") String token, @Path("id") String id);

    //所有公告列表
    @POST("noticeMessage/getList")
    Observable<BaseResponse<String>> getNoticeList(@Header("Authorization") String token);

    //当前用户公告列表
    @GET("noticeMessage/getCurrentUserList")
    Observable<BaseResponse<String>> getCurrentNoticeList(@Header("Authorization") String token);

    //阅读公告
    @GET("noticeMessage/read/{id}")
    Observable<BaseResponse<String>> readNotify(@Header("Authorization") String token, @Path("id") String id);

    //查询公告详情
    @GET("noticeMessage/getById/{id}")
    Observable<BaseResponse<String>> getNotifyById(@Header("Authorization") String token, @Path("id") String id);

    //扫码医废详情
    @POST("wasteBase/getSingleInStorageInfo")
    Observable<BaseResponse<String>> getSingleInStorageInfo(@Header("Authorization") String token, @Body Map<String, Object> map);

    //医废扫码入库
    @POST("wasteBase/putSingleInStorage")
    Observable<BaseResponse<String>> putSingleInStorage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //批量入库医废列表详情
    @POST("wasteBase/putMultipleInStorageInfo")
    Observable<BaseResponse<String>> putMultipleInStorageInfo(@Header("Authorization") String token, @Body Map<String, Object> map);

    //批量入库
    @POST("wasteBase/putMultipleInStorage")
    Observable<BaseResponse<String>> putMultipleInStorage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //箱袋绑定入库医废列表详情
    @POST("wasteBase/getBoxInStorageInfo")
    Observable<BaseResponse<String>> getBoxInStorageInfo(@Header("Authorization") String token, @Body Map<String, Object> map);

    //箱袋绑定入库
    @POST("wasteBase/putBoxInStorage")
    Observable<BaseResponse<String>> putBoxInStorage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //箱袋绑定
    @POST("wasteBase/bindBagWithBox")
    Observable<BaseResponse<String>> bindBagWithBox(@Header("Authorization") String token, @Body Map<String, Object> map);

    //待入库列表
    @POST("wasteBase/getPendingInStorageList")
    Observable<BaseResponse<String>> getPendingInStorageList(@Header("Authorization") String token, @Body Map<String, Object> map);

    //解除绑定
    @POST("wasteBase/removeBindBagWithBox")
    Observable<BaseResponse<String>> removeBindBagWithBox(@Header("Authorization") String token, @Body Map<String, Object> map);

    //根据医废箱ID查询医废列表
    @GET("wasteBase/getListByBoxId/{boxId}")
    Observable<BaseResponse<String>> getListByBoxId(@Header("Authorization") String token, @Path("boxId") String id);

    //扫医废码出库详情
    @POST("wasteBase/getInfoByQrCode")
    Observable<BaseResponse<String>> getInfoByQrCode(@Header("Authorization") String token, @Body Map<String, Object> map);

    //扫医废箱码出库详情
    @POST("wasteBase/getListByBoxQrCode")
    Observable<BaseResponse<String>> getListByBoxQrCode(@Header("Authorization") String token, @Body Map<String, Object> map);

    //扫码出库
    @POST("wasteBase/putSingleOutStorage")
    Observable<BaseResponse<String>> putSingleOutStorage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //接收人列表
    @POST("sysUserRevicer/getList")
    Observable<BaseResponse<String>> getReceiverList(@Header("Authorization") String token);

    //保存接收人
    @POST("sysUserRevicer/save")
    Observable<BaseResponse<String>> saveReceiver(@Header("Authorization") String token, @Body Map<String, Object> map);

    //接收人详情
    @GET("sysUserRevicer/getById/{id}")
    Observable<BaseResponse<String>> getReceiverById(@Header("Authorization") String token, @Path("id") String id);

    @DELETE("sysUserRevicer/remove/{id}")
    Observable<BaseResponse<String>> removeReceiver(@Header("Authorization") String token, @Path("id") String id);

    //默认接收人
    @POST("sysUserRevicer/getDefault")
    Observable<BaseResponse<String>> getDefault(@Header("Authorization") String token);

    //设置默认接收人
    @GET("sysUserRevicer/setDefault/{id}")
    Observable<BaseResponse<String>> setDefault(@Header("Authorization") String token, @Path("id") String id);

    //获取打印信息
    @POST("wasteBase/getPrintMessage")
    Observable<BaseResponse<String>> getPrintMessage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //重量警告
    @POST("wasteBase/checkWarn")
    Observable<BaseResponse<String>> checkWarn(@Header("Authorization") String token, @Body Map<String, Object> map);

    //获取二维码类型
    @POST("wasteBase/getOrCodeType")
    Observable<BaseResponse<String>> getQrCodeType(@Header("Authorization") String token, @Body Map<String, Object> map);

    //打印信息
    @POST("wasteBase/printMessage")
    Observable<BaseResponse<String>> printMessage(@Header("Authorization") String token, @Body Map<String, Object> map);

    //获取待出库列表
    @GET("wasteBase/getPendingOutStorageList")
    Observable<BaseResponse<String>> getPendingOutStorageList(@Header("Authorization") String token);

    //医废追溯列表
    @POST("wasteBase/getList")
    Observable<BaseResponse<String>> getHistory(@Header("Authorization") String token, @Body Map<String, Object> map);

    //医废追溯记录列表
    @GET("wasteBase/getRecordList")
    Observable<BaseResponse<String>> getRecordList(@Header("Authorization") String token, @Query("wasteBaseId") String wasteBaseId);

    //医废出入库记录
    @POST("wasteBase/getInOrOutList")
    Observable<BaseResponse<String>> getInOrOutList(@Header("Authorization") String token, @Body Map<String, Object> map);

    //获取科室列表
    @GET("wasteBase/getDepartByParentId")
    Observable<BaseResponse<String>> getDepartByHospitalBaseId(@Header("Authorization") String token, @Query("parentId") String parentId);

    //根据医废编号查医废详情
    @POST("wasteBase/getListByCodes")
    Observable<BaseResponse<String>> getListByCodes(@Header("Authorization") String token, @Body Map<String, Object> map);

    //根据RFID查医废详情
    @POST("wasteBase/getListByRFID")
    Observable<BaseResponse<String>> getListByRfid(@Header("Authorization") String token, @Body Map<String, Object> map);

    //批量出入库查医废详情
    @POST("wasteBase/getListWithWeightByIds")
    Observable<BaseResponse<String>> getListWithWeightByIds(@Header("Authorization") String token, @Body Map<String, Object> map);

    //医废追溯-医废记录列表
    @POST("wasteBase/getRecordListByQrCode")
    Observable<BaseResponse<String>> getRecordListByQrCode(@Header("Authorization") String token, @Body Map<String, Object> map);

    //条码打印医废标签列表
    @GET("wasteBase/getCurrentCollectList")
    Observable<BaseResponse<String>> getCurrentCollectList(@Header("Authorization") String token);

    //条码打印集装箱标签列表
    @POST("wasteBox/getList")
    Observable<BaseResponse<String>> getBoxLabelList(@Header("Authorization") String token, @Body Map<String, Object> map);

    //新增编辑医废箱
    @POST("wasteBox/save")
    Observable<BaseResponse<String>> saveBoxLabel(@Header("Authorization") String token, @Body Map<String, Object> map);

    //告警列表
    @POST("warnBase/getList")
    Observable<BaseResponse<String>> getAlarmList(@Header("Authorization") String token, @Body Map<String, Object> map);

    //获取医废箱详情
    @GET("wasteBox/getById/{id}")
    Observable<BaseResponse<String>> getBoxInfo(@Header("Authorization") String token, @Path("id") String id);

    //告警详情
    @GET("warnBase/getWarnTimeAndWeight/{wasteBaseId}")
    Observable<BaseResponse<String>> getWarnTimeAndWeight(@Header("Authorization") String token, @Path("wasteBaseId") String id);

    //预警处理
    @POST("warnBase/dealWith")
    Observable<BaseResponse<String>> dealWith(@Header("Authorization") String token, @Body Map<String, Object> map);

    //当前医院用户
    @GET("noticeReceive/getAllHospitalBaseStaff")
    Observable<BaseResponse<String>> getAllHospitalBaseStaff(@Header("Authorization") String token);

    //新增编辑公告
    @POST("noticeMessage/save")
    Observable<BaseResponse<String>> saveNotify(@Header("Authorization") String token, @Body Map<String, Object> map);

    //上传图片
    @Multipart
    @POST("file/uploadFile")
    Observable<BaseResponse<String>> upload(@Header("Authorization") String token, @Part MultipartBody.Part file);

    //上传头像
    @Multipart
    @POST("file/uploadHead")
    Observable<BaseResponse<String>> uploadHead(@Header("Authorization") String token, @Part MultipartBody.Part file);

    //当前医院下的角色
    @GET("noticeMessage/getRoleList")
    Observable<BaseResponse<String>> getRoleList(@Header("Authorization") String token);

    //检查版本更新
    @GET("file/checkUpdate")
    Observable<BaseResponse<String>> checkUpdate();

    //箱出库超重告警详情
    @GET("wasteBase/warnInfoByBoxRecordId")
    Observable<BaseResponse<String>> warnInfoByBoxRecordId(@Header("Authorization") String token, @Query("boxRecordId") String boxRecordId);

    //获取出库单列表
    @POST("wasteBase/getWasteOutList")
    Observable<BaseResponse<String>> getWasteOutList(@Header("Authorization") String token, @Body Map<String, Object> map);

    //获取出库单详情
    @GET("wasteBase/getWasteOutInfo")
    Observable<BaseResponse<String>> getWasteOutInfo(@Header("Authorization") String token, @Query("wasteOutId") String wasteOutId);

}
