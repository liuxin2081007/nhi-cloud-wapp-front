package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BatchOutBean;
import net.zigin.nhi.waste.bean.RfidBatchBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.view.depotout.BatchItemOutActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.paperdb.Paper;

public class RfidBatchAdapter extends RecyclerView.Adapter<RfidBatchAdapter.ViewHolder>{

    public List<RfidBatchBean> list;
    private IActivityUpData dd;
    private boolean isItemCheck = false;

    private Set<RfidBatchBean> newList;

    public RfidBatchAdapter(List<RfidBatchBean> list, IActivityUpData dd) {
        this.list = list;
        this.dd = dd;
        newList = new HashSet<>();
    }

    public void notifyData(List<RfidBatchBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rfid_batch, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RfidBatchBean bean = list.get(position);
        holder.mWeight.setText(bean.getWeight());
        holder.mRfidCode.setText(bean.getRfid());
        holder.mWasteNum.setText(bean.getCode());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mConsignee.setText(bean.getCollectUserStaffName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mTime.setText(bean.getCreateTime());

        boolean isCheck = bean.isSelect();
        if (isCheck) {
            isItemCheck = true;
            holder.mImg.setImageResource(R.drawable.depot_in_selected);
        } else {
            isItemCheck = false;
            holder.mImg.setImageResource(R.drawable.depot_in_unselected);
        }

        newList.add(bean);
        Paper.book().write(Constants.NEW_RFID_BATCH_LIST, newList);

        holder.mLayout.setOnClickListener(v -> {
            if (isItemCheck) {
                newList.remove(bean);
                isItemCheck = false;
                holder.mImg.setImageResource(R.drawable.depot_in_unselected);
                bean.setSelect(false);
                dd.upDataUi();

                newList.add(bean);
                Paper.book().write(Constants.NEW_RFID_BATCH_LIST, newList);
            } else {
                newList.remove(bean);
                isItemCheck = true;
                holder.mImg.setImageResource(R.drawable.depot_in_selected);
                bean.setSelect(true);

                newList.add(bean);
                Paper.book().write(Constants.NEW_RFID_BATCH_LIST, newList);
            }
        });

        holder.mDepot.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), BatchItemOutActivity.class);
            intent.putExtra("wasteItemID", bean.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView mWeight;
        private TextView mWasteNum;
        private TextView mRfidCode;
        private TextView mHospitalName;
        private TextView mType;
        private TextView mConsignee;
        private TextView mTime;
        private Button mDepot;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mImg = itemView.findViewById(R.id.img);
            mWeight = itemView.findViewById(R.id.weight);
            mWasteNum = itemView.findViewById(R.id.waste_num);
            mRfidCode = itemView.findViewById(R.id.rfid_code);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mType = itemView.findViewById(R.id.type);
            mConsignee = itemView.findViewById(R.id.consignee);
            mTime = itemView.findViewById(R.id.time);
            mDepot = itemView.findViewById(R.id.depot);
        }
    }
}
