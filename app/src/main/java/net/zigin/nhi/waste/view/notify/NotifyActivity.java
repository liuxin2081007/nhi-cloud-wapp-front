package net.zigin.nhi.waste.view.notify;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.adapter.NotifyAdapter;
import net.zigin.nhi.waste.bean.NotifyBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.DateUtils;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 通知公告界面
 */
public class NotifyActivity extends AppCompatActivity {

    private RecyclerView mRvNotify;
    private Button mAnnouce;
    private NotifyAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private List<NotifyBean> list = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
        initView();
        initListener();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mRvNotify = findViewById(R.id.rv_notify);
        mAnnouce = findViewById(R.id.annouce);

        mRvNotify.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initListener() {
        mAnnouce.setOnClickListener(v -> {
            Intent intent = new Intent(NotifyActivity.this, AnnounceActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotifyList();
    }

    /**
     * 获取公告列表
     */
    private void getNotifyList() {
        list.clear();
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getCurrentNoticeList(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("公告列表", data);
                    list = JSONArray.parseArray(data, NotifyBean.class);
                    if (list != null && list.size() > 0) {
                        adapter = new NotifyAdapter(list);
                        mRvNotify.setAdapter(adapter);
                    } else {
                        mRvNotify.setAdapter(noDataAdapter);
                    }
                } else {
                    mRvNotify.setAdapter(noDataAdapter);
                }
            }
        });
    }
}