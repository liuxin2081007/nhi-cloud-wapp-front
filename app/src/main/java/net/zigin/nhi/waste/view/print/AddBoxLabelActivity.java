package net.zigin.nhi.waste.view.print;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.IActivityUpData;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.adapter.NotifyAdapter;
import net.zigin.nhi.waste.bean.NotifyBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.notify.NotifyActivity;
import net.zigin.nhi.waste.view.widget.DialogManager;
import net.zigin.nhi.waste.view.widget.OfficeDialog;
import net.zigin.nhi.waste.view.widget.WasteTypeDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class AddBoxLabelActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText mWeight;
    private TextView mUnit;
    private ImageView mScan;
    private TextView mHospitalName;
    private ImageView mArrow;
    private ImageView mArrowType;
    private EditText mRemark;
    private Button mSave;
    private RelativeLayout mDepartNameLayout;
    private EditText mDepartName;
    private RelativeLayout mTypeLayout;
    private EditText mType;

    private String departID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_box_label);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mWeight = findViewById(R.id.weight);
        mUnit = findViewById(R.id.unit);
        mScan = findViewById(R.id.scan);
        mHospitalName = findViewById(R.id.hospitalName);
        mArrow = findViewById(R.id.arrow);
        mArrowType = findViewById(R.id.arrow_type);
        mRemark = findViewById(R.id.remark);
        mSave = findViewById(R.id.save);
        mDepartNameLayout = findViewById(R.id.departName_layout);
        mDepartName = findViewById(R.id.departName);
        mTypeLayout = findViewById(R.id.type_layout);
        mType = findViewById(R.id.type);

        mScan.setOnClickListener(this);
        mDepartNameLayout.setOnClickListener(this);
        mTypeLayout.setOnClickListener(this);
        mSave.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String hospitalName = Paper.book().read(Constants.HOSPITAL_NAME);
        mHospitalName.setText(hospitalName);
    }

    private IDialogUpData dd = new IDialogUpData() {
        @Override
        public void upDataUi(String data, String id) {
            if (StringUtil.isNotEmpty(data)) {
                mDepartName.setText(data);
                departID = id;
            }
        }
    };

    private IDialogUpData typeDD = new IDialogUpData() {
        @Override
        public void upDataUi(String data, String id) {
            if (StringUtil.isNotEmpty(data)) {
                mType.setText(data);
            }
        }
    };

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan:     //科室信息扫码
                break;
            case R.id.departName_layout: //选择科室
                OfficeDialog officeDialog = OfficeDialog.getInstance(this, AddBoxLabelActivity.this, dd);
                officeDialog.show();
                break;
            case R.id.type_layout:     //选择类型
                WasteTypeDialog wasteTypeDialog = WasteTypeDialog.getInstance(this, AddBoxLabelActivity.this, typeDD);
                wasteTypeDialog.show();
                break;
            case R.id.save:     //保存按钮
                if (StringUtil.isEmpty(mWeight.getText().toString()) || StringUtil.isEmpty(mHospitalName.getText().toString())
                        || StringUtil.isEmpty(mDepartName.getText().toString()) || StringUtil.isEmpty(mType.getText().toString())) {
                    Toast.makeText(AddBoxLabelActivity.this, "请填写完整信息！", Toast.LENGTH_SHORT).show();
                } else {
                    Map<String, Object> map = new HashMap<>();
                    switch (mType.getText().toString()) {
                        case "化学性医废":
                            map.put("wasteClassifyCode", "chemical");
                            break;
                        case "感染性医废":
                            map.put("wasteClassifyCode", "infection");
                            break;
                        case "损伤性医废":
                            map.put("wasteClassifyCode", "injury");
                            break;
                        case "病理性医废":
                            map.put("wasteClassifyCode", "pathology");
                            break;
                        case "药物性医废":
                            map.put("wasteClassifyCode", "drug");
                            break;
                    }
                    map.put("hospitalDepartId", departID);
                    map.put("weight", mWeight.getText().toString());
                    if (StringUtil.isNotEmpty(mRemark.getText().toString())) {
                        map.put("remark", mRemark.getText().toString());
                    }
                    Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                            .saveBoxLabel(Paper.book().read(Constants.TOKEN), map);
                    DialogManager.getInstance().showLoading(this);
                    HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                        @Override
                        protected void success(String data) {
                            Log.i("添加集装箱标签", data);
                            Toast.makeText(AddBoxLabelActivity.this, "添加成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OfficeDialog.mInstance = null;
        WasteTypeDialog.mInstance = null;
    }
}