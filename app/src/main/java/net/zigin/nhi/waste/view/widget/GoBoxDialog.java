package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.view.depotout.BoxOutActivity;

public class GoBoxDialog extends Dialog {

    private Button mClose;
    private Button mGo;
    private Activity activity;

    public GoBoxDialog(@NonNull Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_go_box);
        initView();
        initListener();
    }

    private void initView() {
        mClose = findViewById(R.id.close);
        mGo = findViewById(R.id.go);
    }

    private void initListener() {
        mClose.setOnClickListener(v -> {
            dismiss();
        });

        mGo.setOnClickListener(v -> {
            Intent intent = new Intent(activity, BoxOutActivity.class);
            activity.startActivity(intent);
            activity.finish();
        });
    }
}
