package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BoxWasteBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class BoxBindAdapter extends RecyclerView.Adapter<BoxBindAdapter.ViewHolder>{

    public List<BoxWasteBean> list;
    private IActivityUpData dd;
    private List<String> wasteBaseIds;

    public BoxBindAdapter(List<BoxWasteBean> list, IActivityUpData dd) {
        this.list = list;
        this.dd = dd;
        wasteBaseIds = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_box_bind, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BoxWasteBean bean = list.get(position);
        holder.mCollectWeight.setText(bean.getWeight());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mHandover.setText(bean.getHandUserStaffName());
        holder.mCollector.setText(bean.getCollectUserStaffName());
        holder.mTime.setText(bean.getCreateTime());

        holder.mUnbind.setOnClickListener(v -> {
            Map<String, Object> map = new HashMap<>();
            wasteBaseIds.add(bean.getId());
            map.put("wasteBoxId", Paper.book().read(Constants.BOX_ID));
            map.put("wasteBaseIds", wasteBaseIds);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .removeBindBagWithBox(Paper.book().read(Constants.TOKEN), map);
            DialogManager.getInstance().showLoading(v.getContext());
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Toast.makeText(v.getContext(), "解除绑定成功！", Toast.LENGTH_SHORT).show();
                    try {
                        list.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, list.size() - position);
                        dd.upDataUi();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mCollectWeight;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private TextView mHandover;
        private TextView mCollector;
        private TextView mTime;
        private Button mUnbind;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCollectWeight = itemView.findViewById(R.id.collect_weight);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mHandover = itemView.findViewById(R.id.handover);
            mCollector = itemView.findViewById(R.id.collector);
            mTime = itemView.findViewById(R.id.time);
            mUnbind = itemView.findViewById(R.id.unbind);
        }
    }
}
