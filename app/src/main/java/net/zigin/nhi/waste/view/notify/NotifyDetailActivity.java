package net.zigin.nhi.waste.view.notify;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.NotifyDetailAdapter;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 公告详情界面
 */
public class NotifyDetailActivity extends AppCompatActivity {

    private TextView mTitle;
    private TextView mDepartName;
    private TextView mName;
    private TextView mTime;
    private TextView mContent;
    private RecyclerView mRvImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_detail);
        initView();
        getNotifyDetail();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mDepartName = findViewById(R.id.departName);
        mName = findViewById(R.id.name);
        mTime = findViewById(R.id.time);
        mContent = findViewById(R.id.content);
        mRvImg = findViewById(R.id.rv_img);

        mRvImg.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * 获取公告详情
     */
    private void getNotifyDetail() {
        Intent intent = getIntent();
        String id = intent.getStringExtra("notifyID");
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getNotifyById(Paper.book().read(Constants.TOKEN), id);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @SuppressLint("CheckResult")
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("公告详情", data);
                    JSONObject object = JSON.parseObject(data);
                    mTitle.setText(object.getString("title"));
                    mContent.setText(object.getString("content"));
                    mName.setText(object.getString("createUser"));
                    mTime.setText(object.getString("createTime"));
                    mDepartName.setText(object.getString("hospitalDepartName"));

                    List<String> imgBeanList = JSONArray.parseArray(object.getString("picUrls"), String.class);
                    if (imgBeanList != null && imgBeanList.size() > 0) {
                        NotifyDetailAdapter adapter = new NotifyDetailAdapter(imgBeanList);
                        mRvImg.setAdapter(adapter);
                    }
                }
            }
        });
    }
}