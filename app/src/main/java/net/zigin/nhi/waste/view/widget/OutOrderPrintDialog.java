package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class OutOrderPrintDialog extends Dialog {

    private RelativeLayout mSub;
    private EditText mCount;
    private RelativeLayout mAdd;
    private Button mCancel;
    private Button mConfirm;
    private Activity activity;
    private WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    public OutOrderPrintDialog(@NonNull Context context, Activity activity, WebView webView) {
        super(context);
        this.activity = activity;
        this.webView = webView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_print);
        initView();
        initListener();
    }

    private void initView() {
        mSub = findViewById(R.id.sub);
        mCount = findViewById(R.id.count);
        mAdd = findViewById(R.id.add);
        mCancel = findViewById(R.id.cancel);
        mConfirm = findViewById(R.id.confirm);
    }

    private void initListener() {
        mSub.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            if (count > 1) {
                count--;
                mCount.setText(String.valueOf(count));
            } else {
                Toast.makeText(getContext(), "至少打印一份！", Toast.LENGTH_SHORT).show();
            }
        });

        mAdd.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            count++;
            mCount.setText(String.valueOf(count));
        });

        mCancel.setOnClickListener(v -> {
            dismiss();
        });

        mConfirm.setOnClickListener(v -> {
            //根据打印次数循环调用打印方法
            int count = Integer.parseInt(mCount.getText().toString());
            while (count > 0) {
                webView.loadUrl("javascript:scanprint()");
                count --;
            }
            dismiss();
        });
    }
}
