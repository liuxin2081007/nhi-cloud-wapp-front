package net.zigin.nhi.waste;

import android.annotation.SuppressLint;
import android.app.Application;

import com.clj.fastble.BleManager;
import com.clj.fastble.scan.BleScanRuleConfig;
import com.gyf.immersionbar.ImmersionBar;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.olc.scan.ScanManager;
import com.olc.uhf.UhfAdapter;
import com.olc.uhf.UhfManager;

import net.zigin.nhi.waste.view.collect.WasteCollectActivity;
import net.zigin.nhi.waste.view.main.LoginActivity;

import io.paperdb.Paper;
import me.goldze.mvvmhabit.base.BaseApplication;
import me.goldze.mvvmhabit.crash.CaocConfig;
import me.goldze.mvvmhabit.utils.KLog;

public class MainApplication extends BaseApplication {

    private static MainApplication sInstance;
    public static UhfManager mService;
    public static int m_version = 2;
    public static ScanManager scanManager;

    public static MainApplication getInstance() {
        return sInstance;
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        KLog.init(true);
        Paper.init(this);
        //配置全局异常崩溃操作
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //背景模式,开启沉浸式
                .enabled(true) //是否启动全局异常捕获
                .showErrorDetails(true) //是否显示错误详细信息
                .showRestartButton(true) //是否显示重启按钮
                .trackActivities(true) //是否跟踪Activity
                .minTimeBetweenCrashesMs(2000) //崩溃的间隔时间(毫秒)
                .errorDrawable(R.mipmap.ic_launcher) //错误图标
                .restartActivity(LoginActivity.class) //重新启动后的activity
                //.errorActivity(YourCustomErrorActivity.class) //崩溃后的错误activity
                //.eventListener(new YourCustomEventListener()) //崩溃后的错误监听
                .apply();

        //初始化扫码
        scanManager = (ScanManager) getSystemService("olc_service_scan");
        //设置扫描模式为广播模式
        if (scanManager != null) {
            scanManager.setBarcodeReceiveModel(2);
        }

        //初始化Ble蓝牙框架
        BleManager.getInstance().init(sInstance);
        BleManager.getInstance()
                .enableLog(true)
                .setReConnectCount(1, 8000)
                .setOperateTimeout(8000);
        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder().setScanTimeOut(8000).build();
        BleManager.getInstance().initScanRule(scanRuleConfig);

        //初始化科大讯飞语音
        StringBuffer param = new StringBuffer();
        param.append("appid=" + getString(R.string.app_id));
        param.append(",");
        // 设置使用v5+
        param.append(SpeechConstant.ENGINE_MODE + "=" + SpeechConstant.MODE_MSC);
        SpeechUtility.createUtility(sInstance, param.toString());

        //初始化RFID
        mService = UhfAdapter.getUhfManager(sInstance);
        if (mService != null) {
            int status = mService.getStatus();
            m_version = ((status & 0xff0000) >> 16) & 0xff;
            if (mService.open()) {
                int b = 0;
            }
            boolean isOpen = mService.open();
        }

    }
}
