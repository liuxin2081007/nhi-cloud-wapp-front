package net.zigin.nhi.waste.view.print;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;

/**
 * 条码打印界面
 */
public class CodePrintActivity extends AppCompatActivity {

    private LinearLayout mWasteLayout;
    private LinearLayout mBoxLayout;
    private LinearLayout mOrderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_print);
        initView();
        initListener();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mWasteLayout = findViewById(R.id.waste_layout);
        mBoxLayout = findViewById(R.id.box_layout);
        mOrderLayout = findViewById(R.id.order_layout);
    }

    private void initListener() {
        mWasteLayout.setOnClickListener(v -> {
            Intent intent = new Intent(CodePrintActivity.this, WasteLabelActivity.class);
            startActivity(intent);
        });

        mBoxLayout.setOnClickListener(v -> {
            Intent intent = new Intent(CodePrintActivity.this, BoxLabelActivity.class);
            startActivity(intent);
        });

        mOrderLayout.setOnClickListener(v -> {
            Intent intent = new Intent(CodePrintActivity.this, OutOrderActivity.class);
            startActivity(intent);
        });
    }
}