package net.zigin.nhi.waste.bean;

/**
 * RFID批量出库医废详情接口bean
 */
public class RfidBatchBean {
    private String ckCode;
    private String code;
    private String collectUserStaffId;
    private String collectUserStaffName;
    private String createTime;
    private String createUser;
    private String handUserStaffId;
    private String handUserStaffName;
    private String hospitalBaseId;
    private String hospitalBaseName;
    private String hospitalDepartId;
    private String hospitalDepartName;
    private String hospitalPlaceId;
    private String hospitalPlaceName;
    private String id;
    private String modifyTime;
    private String modifyUser;
    private String remark;
    private String rfid;
    private String status;
    private String userRevicerName;
    private String warnType;
    private String wasteBoxCode;
    private String wasteBoxId;
    private String wasteBoxRecordId;
    private String wasteClassifyCode;
    private String wasteClassifyName;
    private String weight;

    private boolean isSelect;  //是否全选

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHandUserStaffName() {
        return handUserStaffName;
    }

    public void setHandUserStaffName(String handUserStaffName) {
        this.handUserStaffName = handUserStaffName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getHandUserStaffId() {
        return handUserStaffId;
    }

    public void setHandUserStaffId(String handUserStaffId) {
        this.handUserStaffId = handUserStaffId;
    }

    public String getHospitalBaseId() {
        return hospitalBaseId;
    }

    public void setHospitalBaseId(String hospitalBaseId) {
        this.hospitalBaseId = hospitalBaseId;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartId() {
        return hospitalDepartId;
    }

    public void setHospitalDepartId(String hospitalDepartId) {
        this.hospitalDepartId = hospitalDepartId;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getCollectUserStaffId() {
        return collectUserStaffId;
    }

    public void setCollectUserStaffId(String collectUserStaffId) {
        this.collectUserStaffId = collectUserStaffId;
    }

    public String getCollectUserStaffName() {
        return collectUserStaffName;
    }

    public void setCollectUserStaffName(String collectUserStaffName) {
        this.collectUserStaffName = collectUserStaffName;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCkCode() {
        return ckCode;
    }

    public void setCkCode(String ckCode) {
        this.ckCode = ckCode;
    }

    public String getHospitalPlaceId() {
        return hospitalPlaceId;
    }

    public void setHospitalPlaceId(String hospitalPlaceId) {
        this.hospitalPlaceId = hospitalPlaceId;
    }

    public String getHospitalPlaceName() {
        return hospitalPlaceName;
    }

    public void setHospitalPlaceName(String hospitalPlaceName) {
        this.hospitalPlaceName = hospitalPlaceName;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public String getUserRevicerName() {
        return userRevicerName;
    }

    public void setUserRevicerName(String userRevicerName) {
        this.userRevicerName = userRevicerName;
    }

    public String getWarnType() {
        return warnType;
    }

    public void setWarnType(String warnType) {
        this.warnType = warnType;
    }

    public String getWasteBoxCode() {
        return wasteBoxCode;
    }

    public void setWasteBoxCode(String wasteBoxCode) {
        this.wasteBoxCode = wasteBoxCode;
    }

    public String getWasteBoxId() {
        return wasteBoxId;
    }

    public void setWasteBoxId(String wasteBoxId) {
        this.wasteBoxId = wasteBoxId;
    }

    public String getWasteBoxRecordId() {
        return wasteBoxRecordId;
    }

    public void setWasteBoxRecordId(String wasteBoxRecordId) {
        this.wasteBoxRecordId = wasteBoxRecordId;
    }
}
