package net.zigin.nhi.waste.view.history;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.MainApplication;
import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.HistoryAdapter;
import net.zigin.nhi.waste.bean.HistoryBean;
import net.zigin.nhi.waste.bean.RecordBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.scan.CodeListener;
import net.zigin.nhi.waste.scan.CodeReceiver;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotin.DepotInActivity;
import net.zigin.nhi.waste.view.main.LoginActivity;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 扫医废码追溯界面
 */
public class ScanRecordActivity extends AppCompatActivity implements CodeListener {

    private CodeReceiver receiver;
    private LinearLayout mScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_record);

        initView();
        initReceiver();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }

    private void initReceiver() {
        receiver = new CodeReceiver();
        receiver.setListener(this);
        registerReceiver(receiver, new IntentFilter("com.barcode.sendBroadcast"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (MainApplication.scanManager != null) {
                //开启扫描
                Intent intent = new Intent();
                intent.setAction("com.barcode.sendBroadcastScan");
                sendBroadcast(intent);
            } else {
                Intent intent = new Intent(ScanRecordActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 医废追溯查扫码具体
     * @param data 扫码结果
     */
    private void getDetail(String data) {
        if (StringUtil.isNotEmpty(data)) {
            Map<String, Object> map = new HashMap<>();
            map.put("content", data);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getRecordListByQrCode(Paper.book().read(Constants.TOKEN), map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String result) {
                    if (StringUtil.isNotEmpty(result)) {
                        Log.i("医废追溯查扫码具体", result);
                        List<RecordBean> list = JSONArray.parseArray(result, RecordBean.class);
                        Paper.book().write("record_list", list);
                        Intent intent = new Intent(ScanRecordActivity.this, RecordDetailActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
    }

    @Override
    public void getData(String data) {
        Log.i("扫码结果", data);
        getDetail(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                getDetail(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
            System.gc();
        }
        super.onDestroy();
    }
}