package net.zigin.nhi.waste.utils;

import android.annotation.SuppressLint;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式工具类
 */
public class DateUtils {

    /**
     * 设置时间格式
     * @param strDate
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public Date strToDateLong(String strDate) {
         SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ParsePosition pos = new ParsePosition(0);
        return formatter.parse(strDate, pos);
    }

    /**
     * <Enter>
     * 指定日期格式
     *
     * @param seconds
     * @param format
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public String timeStamp2Date(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) {
            format = "yyyy-MM-dd";
        }
         SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.parseLong(seconds + "000")));
    }

    /**
     * 设置时间格式为-----多久之前
     * @param inTime
     * @return
     */
    public String timeUtil(Date inTime) {
        // 拿到当前时间戳和发布时的时间戳，然后得出时间戳差
        Date curTime = new Date();
        long timeDiff = curTime.getTime() - inTime.getTime();
        //上面一行代码可以换成以下（兼容性的解决）

        // 单位换算
        long min = 60 * 1000;
        long hour = min * 60;
        long day = hour * 24;
        long week = day * 7;
        long month = week * 4;
        long year = month * 12;
        DecimalFormat df = new DecimalFormat("#");
        // 计算发布时间距离当前时间的周、天、时、分
        double exceedyear = Math.floor(timeDiff / year);
        double exceedmonth = Math.floor(timeDiff / month);
        double exceedWeek = Math.floor(timeDiff / week);
        double exceedDay = Math.floor(timeDiff / day);
        double exceedHour = Math.floor(timeDiff / hour);
        double exceedMin = Math.floor(timeDiff / min);


        // 最后判断时间差到底是属于哪个区间，然后return

        if (exceedyear < 100 && exceedyear > 0) {
            return df.format(exceedyear) + "年前";
        } else {
            if (exceedmonth < 12 && exceedmonth > 0) {
                return df.format(exceedmonth) + "月前";
            } else {
                if (exceedWeek < 4 && exceedWeek > 0) {
                    return df.format(exceedWeek) + "星期前";
                } else {
                    if (exceedDay < 7 && exceedDay > 0) {
                        return df.format(exceedDay) + "天前";
                    } else {
                        if (exceedHour < 24 && exceedHour > 0) {
                            return df.format(exceedHour) + "小时前";
                        } else {
                            return df.format(exceedMin) + "分钟前";
                        }
                    }
                }
            }
        }
    }

    /**
     * <Enter>
     * String转日期
     *
     * @param dateString
     * @param pattern
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public long getStringToDate(String dateString, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime() / 1000;
    }

    /**
     *
     * @param mss 要转换的毫秒数
     * @return 该毫秒数转换为 * days * hours * minutes * seconds 后的格式
     * @author fy.zhang
     */
    public String formatDuring(long mss) {
        long days = mss / (1000 * 60 * 60 * 24);
        long hours = (mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
        long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
        long seconds = (mss % (1000 * 60)) / 1000;
        return days + " 天 " + hours + " 时 " + minutes + " 分 "
                + seconds + " 秒 ";
    }
}
